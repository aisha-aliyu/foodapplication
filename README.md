# Food Recipe Application

## Clone repository

Use these steps to clone from SourceTree, our client for using the repository command-line free. Cloning allows you to work on your files locally. If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/). If you prefer to clone from the command line, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).

1. You’ll see the clone button under the **Source** heading. Click that button.
2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you’d like to and then click **Clone**.
4. Open the directory you just created to see your repository’s files.

## Folder Structure
**src** - Source roots for all files and directories.
**src/app** - Source roots for main application files and directories.
**src/app/configurations** - Source roots for credentials and routes.
**src/app/model** - Source roots for object classes.
**src/app/pages** - Source roots for pages.
**src/app/providers** - Source roots for api providers.
**src/app/types** - Source roots for types.
**src/assets** - Source roots for images, icons.
**src/environment** - Source roots for environment declaration.
**src/theme** - Source roots for variable settings like color, e.t.c.