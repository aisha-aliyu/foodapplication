import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

const routes: Routes = [
    {path: '', redirectTo: 'home', pathMatch: 'full'},
    {path: 'home', loadChildren: './pages/home/home.module#HomePageModule'},
    {path: 'login', loadChildren: './pages/login/login.module#LoginPageModule'},
    {path: 'register', loadChildren: './pages/register/register.module#RegisterPageModule'},
    {path: 'forgot-password', loadChildren: './pages/forgot-password/forgot-password.module#ForgotPasswordPageModule'},
    {path: 'terms', loadChildren: './pages/termsandconditions/termsandconditions.module#TermsandconditionsPageModule'},
    {path: 'viewalluser', loadChildren: './pages/viewalluser/viewalluser.module#ViewAllUserPageModule'},
    {path: 'popups', loadChildren: './partials/popups/popups.module#PopupsPageModule'},
    {path: 'allrecipes', loadChildren: './pages/allrecipes/allrecipes.module#AllrecipesPageModule'},
    {path: 'addrecipes', loadChildren: './pages/addrecipes/addrecipes.module#AddrecipesPageModule'},
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
