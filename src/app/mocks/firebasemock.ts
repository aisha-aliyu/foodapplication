import {Observable} from 'rxjs/index';
import {Users} from '../model/users/users';
import UserCredential = firebase.auth.UserCredential;
import User = firebase.User;
import {promise} from 'selenium-webdriver';
import {validate} from 'codelyzer/walkerFactory/walkerFn';
import * as AllRecipesMock from '../mocks/allRecipesMocks.json';
import {Recipes} from '../model/recipes/recipes';

export class FirebaseMock {

    public user: Users = {
        role: 'user',
        email: 'test@gmail.com',
        displayName: 'Test User',
        firstName: 'Test',
        lastName: 'User',
        photoURL: '',
        uid: '12345',
        suggested: 3,
        favorites: 5
    } as Users;
    public recipes: Recipes[] = AllRecipesMock.recipes as Recipes[];

    public userInfoList: Users[] = [this.user];
    public userInfo: Users = null;
    public userCreds: any = {'user': this.user};
    public password = 'rest';

    constructor() {
    }

    public initializeApp(value: any) {

    }

    public validateEmail(email) {
        const re = /\S+@\S+\.\S+/;
        return re.test(String(email).toLowerCase());
    }

    public auth() {
        const self = this;
        return {
            currentUser: self.userInfo,
            signOut() {
                self.userInfo = null;
            },
            onAuthStateChanged(callback) {
                callback(self.userInfo);
            },
            getAllRecipes() {},
            createUserAndRetrieveDataWithEmailAndPassword(email, password) {
                return new Promise<UserCredential | any>((resolve, reject) => {
                    if (email !== self.user.email) {
                        if (self.password === password) {
                            resolve({
                                additionalUserInfo: null,
                                credential: null,
                                operationType: null,
                                user: {uid: 'Qdjtf568779lgygkyj'}
                            } as UserCredential);
                        } else {
                            reject({code: 1235, message: 'Incorrect password'});
                        }
                    } else {
                        reject({code: 1235, message: 'user already exists in the database. '});
                    }
                });
            },
            signInWithEmailAndPassword(email, password): Promise<UserCredential> {
                return new Promise<UserCredential>((resolve, reject) => {
                    if ((email === '') || (password === '')) {
                        reject({code: 0o11, message: 'Please fill out all fields'});
                    } else if ((email !== self.user.email) || (password !== self.password)) {
                        reject({
                            code: 0o14,
                            message: 'There is no user record corresponding to this identifier. The user may have been deleted.'
                        });
                    } else {
                        self.userCreds.delete = {};
                        resolve({
                            additionalUserInfo: null,
                            credential: null,
                            operationType: 'signIn',
                            user: self.userCreds['user']
                        } as UserCredential);
                    }
                });
            },
            sendPasswordResetEmail(email: string): Promise<void> {
                return new Promise((resolve, reject) => {
                    if (!self.validateEmail(email)) {
                        reject({
                            code: 'auth/invalid-email',
                            message: 'The email address is badly formatted.'
                        });
                    } else if (email !== self.user.email) {
                        reject({
                            code: 'auth/user-not-found',
                            message: 'There is no user record corresponding to this identifier. The user may have been deleted.'
                        });
                    } else {
                        resolve();
                    }
                });
            }
        };
    }

    public database() {
        const self = this;
        return {
            ref(path: string) {
                return {
                    orderByChild(value) {
                        return {
                            equalTo(actualValue) {
                                return {
                                    on(expects, callback) {
                                        if (expects === 'value') {
                                            callback({
                                                val() {
                                                    if (self.userInfo) {
                                                        const returnvalue = {};
                                                        returnvalue[self.userInfo.uid] = self.userInfo;
                                                        return returnvalue;
                                                    } else {
                                                        return null;
                                                    }
                                                }
                                            });
                                        }
                                    }
                                };
                            }
                        };
                    },
                    on(expects, callback) {
                        if (expects === 'value') {
                            callback({
                                val() {
                                    return self.userInfoList && self.userInfoList.length > 0 ? self.userInfoList : null;
                                }
                            });
                        }
                    },
                    child(value: string) {
                        return {
                            once(expects, callback) {
                                if (expects === 'value') {
                                    callback({
                                        val() {
                                            return self.userInfoList && self.userInfoList.length > 0 ? self.userInfoList : null;
                                        }
                                    });
                                }
                            },
                            set(object: Users) {
                                self.userInfoList = [object];
                                return Observable;
                            },
                            update(object: Users) {
                                self.userInfoList[0].firstName = object.firstName;
                                self.userInfoList[0].lastName = object.lastName;
                                self.userInfoList[0].email = object.email;
                                self.userInfoList[0].role = object.role;
                                return Observable;
                            },
                            remove(uid) {
                                self.userInfoList = [];
                            }
                        };
                    }
                };
            }
        };
    }

    public recipeDatabase() {
        const self = this;
        return {
            ref(path: string) {
                return {
                    orderByChild(value) {
                        return {
                            equalTo(actualValue) {
                                return {
                                    on(expects, callback) {
                                        if (expects === 'value') {
                                            callback({
                                                val() {
                                                    if (self.userInfo) {
                                                        const returnvalue = {};
                                                        returnvalue[self.userInfo.uid] = self.userInfo;
                                                        return returnvalue;
                                                    } else {
                                                        return null;
                                                    }
                                                }
                                            });
                                        }
                                    }
                                };
                            }
                        };
                    },
                    on(expects, callback) {
                        if (expects === 'value') {
                            callback({
                                val() {
                                    return self.recipes && self.recipes.length > 0 ? self.recipes : null;
                                }
                            });
                        }
                    },
                    child(value: string) {
                        return {
                            once(expects, callback) {
                                if (expects === 'value') {
                                    callback({
                                        val() {
                                            return self.recipes && self.recipes.length > 0 ? self.recipes : null;
                                        }
                                    });
                                }
                            },
                            set(object: Users) {
                                self.userInfoList = [object];
                                return Observable;
                            },
                            remove() {
                                self.recipes = self.recipes.filter(function(val, index, arr) {

                                    return val.id !== value;

                                });
                                return self.recipes;
                            }
                        };
                    }
                };
            }
        };
    }
}

