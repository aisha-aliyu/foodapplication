import {Location, LocationStrategy} from '@angular/common';

export class CookieServiceMock {
    public cname = null;
    public cvalue = null;
    public exdays = null;

    constructor() {
    }

    public get(cname: string) {
        return this.check(cname) ? this.cvalue : '';
    }

    public check(cname: string) {
        return cname === this.cname;
    }

    public set(cname: string, cvalue: string, exdays: number) {
        this.cname = cname;
        this.cvalue = cvalue;
        this.exdays = exdays;
        return;
    }

    public delete(cname: string) {
        this.cname = null;
        this.cvalue = null;
        this.exdays = null;
        return;
    }
}

export class LocationMock {
    public location: Location;

    constructor() {
    }

    public navigateForward(path: string) {
        this.location.go(path);
    }

    public navigateByUrl(path: string) {
        this.location.go(path);
    }

    public goBack() {
        this.location.back();
    }
}

export class NavParamsMock {
    private _data = {};

    constructor() {
    }

    get(param) {
        return this._data[param];
    }

    get data(): {} {
        return this._data;
    }

    set data(value: {}) {
        this._data = value;
    }
}


export class PopupMock {
    view: any = '';

    constructor() {
    }

    public async create(param: any) {
        const self = this;
        return await {
            present() {
                return self.view;
            }
        };
    }

    public dismiss() {
        this.view = '';
    }
}
