import {Injectable} from '@angular/core';

/*Providers Import*/
import * as firebase from 'firebase/app';
import 'firebase/database';
import {Users} from '../../model/users/users';
import * as routes from '../../configurations/routes.json';
import {Recipes} from '../../model/recipes/recipes';

@Injectable({
    providedIn: 'root'
})
export class DatabaseService {
    databaseConnect;
    userRef;
    recipeRef;

    constructor() {
        this.databaseConnect = firebase.database();
        this.userRef = this.databaseConnect.ref(routes['users']);
        this.recipeRef = this.databaseConnect.ref(routes['recipes']);
    }

    /*Get all firebase users*/
    getUserDetails() {
        return this.userRef;
    }

    /*Get a specific firebase users*/
    getSpecificUserDetails(uid) {
        return this.userRef.orderByChild('uid').equalTo(uid);
    }

    /*Save new user's role and name based on firebase user id*/
    saveUserDetails(user: Users) {
        const childRef = this.userRef.child(user.uid);
        return childRef.once('value', (snapshot) => {
            // If user does not exists already set their role as user
            if (snapshot.val() === null) {
                return this.userRef.child(user.uid).set(user);
            }
        });
    }

    updateUserDetails(user: Users) {
        return this.userRef.child(user.uid).update({
            'firstName': user.firstName,
            'lastName': user.lastName,
            'email': user.email,
            'role': user.role,
        });
    }

    deleteUser(uid) {
        return this.userRef.child(uid).remove();
    }

    /*Save new recipe based on id*/
    saveRecipe(recipe: Recipes) {
        return this.recipeRef.child(recipe.id).set(recipe);
    }

    /*Get all recipes*/
    getAllRecipes() {
        return this.recipeRef;
    }

    deleteRecipe(recipe: Recipes) {
        return this.recipeRef.child(recipe.id).remove();
    }


}
