import {TestBed} from '@angular/core/testing';

import {DatabaseService} from './database.service';
import {FirebaseMock} from '../../mocks/firebasemock';
import * as AllRecipesMock from '../../mocks/allRecipesMocks.json';
import * as firebase from 'firebase/app';
import 'firebase/database';
import {Recipes} from '../../model/recipes/recipes';

describe('DatabaseService', () => {
    let service: DatabaseService;
    let mockFirebase: FirebaseMock = new FirebaseMock();
    let firebaseConnect;

    beforeEach(() => {
        service = new DatabaseService();
        mockFirebase.userInfoList = null;
    });

    beforeAll(() => {
        firebaseConnect = spyOn(firebase, 'database').and.returnValue(mockFirebase.database());
    });

    // Test service creation succeeded
    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    // Test get user details
    it('should get user details', () => {
        expect(service).toBeTruthy();
        service.getUserDetails().on('value', function (snapshot) {
            expect(firebaseConnect).toHaveBeenCalled();
            expect(snapshot.val()).toBe(null);
            mockFirebase.userInfoList = [mockFirebase.user];
            expect(snapshot.val()).toBe(mockFirebase.userInfoList);
        });
    });

    // Test setting user details if user does not exist.
    it('should set user details if not exists', () => {
        expect(mockFirebase.userInfoList).toBeNull();
        service.saveUserDetails(mockFirebase.user);
        expect(firebaseConnect).toHaveBeenCalled();
        expect(mockFirebase.userInfoList.length).toEqual(1);
        expect(mockFirebase.userInfoList[0]).toBe(mockFirebase.user);
    });

    // Test not setting user details if user exist.
    it('should not set user details when exists', () => {
        mockFirebase.userInfoList = [mockFirebase.user];
        expect(mockFirebase.userInfoList.length).toEqual(1);
        service.saveUserDetails(mockFirebase.user);
        expect(firebaseConnect).toHaveBeenCalled();
        expect(mockFirebase.userInfoList.length).toEqual(1);
    });

    // Test updating user detail
    it ('should update user detail', () => {
        mockFirebase.userInfoList = [mockFirebase.user];
        expect(mockFirebase.userInfoList.length).toEqual(1);
        service.updateUserDetails(mockFirebase.user);
        expect(firebaseConnect).toHaveBeenCalled();
        expect(mockFirebase.userInfoList.length).toEqual(1);
        expect(mockFirebase.userInfoList[0]).toBe(mockFirebase.user);
    });

    // Test deleting user detail
    it ('should delete user', () => {
        mockFirebase.userInfoList = [mockFirebase.user];
        expect(mockFirebase.userInfoList.length).toEqual(1);
        service.deleteUser(mockFirebase.user.uid);
        expect(firebaseConnect).toHaveBeenCalled();
        expect(mockFirebase.userInfoList.length).toEqual(0);
    });

});

describe('DatabaseService', () => {
    let service: DatabaseService;
    let mockFirebase: FirebaseMock = new FirebaseMock();
    let firebaseConnect;

    beforeEach(() => {
        service = new DatabaseService();
        mockFirebase.userInfoList = null;
        mockFirebase.recipes = null;
    });

    beforeAll(() => {
        firebaseConnect = spyOn(firebase, 'database').and.returnValue(mockFirebase.recipeDatabase());
    });


    // Test get all recipes
    it ('should get recipes', () => {
        expect(service).toBeTruthy();
        service.getAllRecipes().on('value', function (snapshot) {
            expect(firebaseConnect).toHaveBeenCalled();
            expect(snapshot.val()).toBe(null);
            mockFirebase.recipes = AllRecipesMock.recipes as Recipes[];
            expect(snapshot.val()).toBe(mockFirebase.recipes);
        });
    });

    // Test get all recipes
    it ('should delete recipe', () => {
        mockFirebase.recipes = AllRecipesMock.recipes as Recipes[];
        expect(mockFirebase.recipes.length).toEqual(8);
        console.log('plpolp')
        service.deleteRecipe(mockFirebase.recipes[3]);
        expect(firebaseConnect).toHaveBeenCalled();
        expect(mockFirebase.recipes.length).toEqual(7);
    });
});
