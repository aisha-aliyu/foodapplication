import {Injectable} from '@angular/core';
import {CookieService} from 'ngx-cookie-service';

/*Providers Import*/
import * as firebase from 'firebase/app';
import 'firebase/auth';
import {DatabaseService} from '../database/database.service';
import {Router} from '@angular/router';
import {Users} from '../../model/users/users';

@Injectable({
    providedIn: 'root'
})

export class AuthenticationService {
    public user: Users;

    constructor(private cookieService: CookieService) {
    }

    setCookie(cname: string, cvalue: string, exdays: number) {
        this.cookieService.set(cname, cvalue, exdays);
    }

    getCookie(cname) {
        return this.cookieService.check(cname) ? this.cookieService.get(cname) : '';
    }

    deleteCookie(cname) {
        this.cookieService.delete(cname);
    }

    checkCookie(cname) {
        return this.cookieService.check(cname);
    }

    /*Send password reset email*/
    sendPasswordResetEmail(email: string) {
        return firebase.auth().sendPasswordResetEmail(email);
    }

    verifyUser(email: string, password: string) {
        return firebase.auth().signInWithEmailAndPassword(email, password);
    }

    facebookLogin() {
        let provider = new firebase.auth.FacebookAuthProvider();
        return firebase.auth().signInWithPopup(provider);
    }

    twitterLogin() {
        let provider = new firebase.auth.TwitterAuthProvider();
        return firebase.auth().signInWithPopup(provider);
    }

    googleLogin() {
        let provider = new firebase.auth.GoogleAuthProvider();
        return firebase.auth().signInWithPopup(provider);
    }

    addEmailAuth(email: string, password: string) {
        return firebase.auth().createUserAndRetrieveDataWithEmailAndPassword(email, password);
    }

    getUser() {
        return new Promise((resolve, reject) => {
            firebase.auth().onAuthStateChanged((user) => {
                if (user) {
                    resolve(user);
                } else {
                    reject(null);
                }
            });
        });
    }

    signOut(router) {
        firebase.auth().signOut();
        router.navigateByUrl('login');
    }
}
