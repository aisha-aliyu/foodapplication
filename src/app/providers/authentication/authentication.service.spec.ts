import {TestBed} from '@angular/core/testing';

import {AuthenticationService} from './authentication.service';
import {CookieServiceMock, LocationMock} from '../../mocks/others';
import {CookieService} from 'ngx-cookie-service';
import {FirebaseMock} from '../../mocks/firebasemock';
import * as firebase from 'firebase/app';
import 'firebase/database';
import {Router} from '@angular/router';

describe('AuthenticationService', () => {
    let service: AuthenticationService;
    let mockFirebase: FirebaseMock = new FirebaseMock();
    let password: string;
    let firebaseAuth;
    let email: string;
    let cname: string;
    let cname1: string;
    let cvalue: string;
    let expire: number;
    const mockNavControl: LocationMock = new LocationMock();


    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [],
            providers: [
                {provide: CookieService, useClass: CookieServiceMock},
                {provide: Router, useValue: mockNavControl},
            ]
        });
    });

    beforeAll(() => {
         firebaseAuth = spyOn(firebase, 'auth').and.returnValue(mockFirebase.auth());
    });

    it('should be created', () => {
        service = TestBed.get(AuthenticationService);
        expect(service).toBeTruthy();
    });

    it('should send message if email or passowrd is empty', () => {
        expect(service).toBeTruthy();
        service.verifyUser('', '').catch(error => {
            expect(firebaseAuth).toHaveBeenCalled();
            expect(error.code).toBe(0o11);
        });
    });


    it('if you email or password is not right', () => {
        expect(service).toBeTruthy();
        service.verifyUser(mockFirebase.user.email, '12345678').catch(error => {
            expect(firebaseAuth).toHaveBeenCalled();
            expect(error.code).toBe(0o14);
        });

        service.verifyUser('t@t.com', mockFirebase.password).catch(error => {
            expect(firebaseAuth).toHaveBeenCalled();
            expect(error.code).toBe(0o14);
        });
    });

    it('login successful', () => {
        expect(service).toBeTruthy();
        let result = 'signIn';
        service.verifyUser(mockFirebase.user.email, mockFirebase.password).then((res) => {
            expect(res.operationType).toBe(result);
        });
    });

    it('should not send reset password if email is malformated', () => {
        expect(service).toBeTruthy();
        email = 'abcde';
        service.sendPasswordResetEmail(email).catch(err => {
            expect(firebaseAuth).toHaveBeenCalled();
            expect(err.code).toBe('auth/invalid-email');
        });
    });

    it('should not send reset password if email not matched', () => {
        expect(service).toBeTruthy();
        email = 'fake@gmail.com';
        service.sendPasswordResetEmail(email).catch(err => {
            expect(firebaseAuth).toHaveBeenCalled();
            expect(err).toBeTruthy();
        });
    });

    it('should send email of resetting password', () => {
        expect(service).toBeTruthy();
        email = 'test@gmail.com';
        expect(firebaseAuth).toHaveBeenCalled();
        service.sendPasswordResetEmail(email).then((res) => {
            expect(res).toBeUndefined();
        });
    });

    it('should set cookie', () => {
        expect(service).toBeTruthy();
        expect(service.checkCookie(cname)).toEqual(false);
        cname = 'fakeCookie';
        cvalue = 'fakeCookieValue';
        expire = 100;
        service.setCookie(cname, cvalue, expire);
        expect(service.checkCookie(cname)).toEqual(true);
    });

    it('should get cookie', () => {
        expect(service).toBeTruthy();
        service.deleteCookie('fakeCookie');
        expect(service.checkCookie(cname)).toEqual(false);
        cname = 'fakeCookie';
        cvalue = 'fakeCookieValue';
        expire = 100;
        service.setCookie(cname, cvalue, expire);
        expect(service.checkCookie(cname)).toEqual(true);
        expect(service.getCookie(cname)).toEqual(cvalue);

    });

    it('should delete cookie', () => {
        expect(service).toBeTruthy();
        cname = 'fakeCookie';
        cvalue = 'fakeCookieValue';
        expire = 100;
        service.setCookie(cname, cvalue, expire);
        expect(service.checkCookie(cname)).toEqual(true);
        service.deleteCookie(cname);
        expect(service.checkCookie(cname)).toEqual(false);
    });

    it('should check cookie', () => {
        expect(service).toBeTruthy();
        expect(service.checkCookie(cname)).toEqual(false);
        cname = 'fakeCookie';
        cvalue = 'fakeCookieValue';
        expire = 100;
        service.setCookie(cname, cvalue, expire);
        expect(service.checkCookie(cname)).toEqual(true);
        cname1 = 'fakeCookie1';
        expect(service.checkCookie(cname1)).toEqual(false);

    });

    it('should fail when user info is null', () => {
        service.getUser().catch(error => {
            expect(firebaseAuth).toHaveBeenCalled();
            expect(error).toBe(null);
        });
    });

});


