import {TestBed} from '@angular/core/testing';

import {ExtrasService} from './extras.service';
import {CookieServiceMock, PopupMock} from '../../mocks/others';
import * as firebase from 'firebase/app';
import {PopoverController} from '@ionic/angular';
import {FirebaseMock} from '../../mocks/firebasemock';
import {CookieService} from 'ngx-cookie-service';
import {LocationMock} from '../../mocks/others';
import {Location} from '@angular/common';
import {Users} from '../../model/users/users';

describe('ExtrasService', () => {
    let service: ExtrasService;
    let popMock;
    const popupMock = new PopupMock();
    const mockFirebase: FirebaseMock = new FirebaseMock();
    let firebaseInitialize;
    let firebaseAuth;
    let firebaseConnect;
    const mockNavControl: LocationMock = new LocationMock();


    beforeAll(() => {
        popMock = spyOn(popupMock, 'create');
        firebaseInitialize = spyOn(firebase, 'initializeApp').and.returnValue(mockFirebase.initializeApp({}));
        firebaseAuth = spyOn(firebase, 'auth').and.returnValue(mockFirebase.auth());
        firebaseConnect = spyOn(firebase, 'database').and.returnValue(mockFirebase.database());
    });

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [],
            providers: [
                {provide: PopoverController, useValue: popupMock}, {provide: CookieService, useClass: CookieServiceMock},
                {provide: Location, useValue: mockNavControl},
            ]
        });

        service = TestBed.get(ExtrasService);
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    it('should call popup', () => {
        service.showPopup({}, {}, false).then(() => {
            expect(popMock).toHaveBeenCalled();
        });
    });

    it('should setup initial user', () => {
        const sampleThis = {user: {}};
        service.initializeUser(sampleThis);
        expect(firebaseAuth).toHaveBeenCalled();
        expect(firebaseConnect).toHaveBeenCalled();
    });

    it('should set rating', () => {
        expect(service.setRating(1, 2)).toEqual(2);
        expect(service.setRating(1, 1)).toEqual(0);
    });

});
