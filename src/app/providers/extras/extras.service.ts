import {Injectable} from '@angular/core';
import {PopoverController} from '@ionic/angular';
import {PopupsPage} from '../../partials/popups/popups.page';
import {Users} from '../../model/users/users';
import {DatabaseService} from '../database/database.service';
import {AuthenticationService} from '../authentication/authentication.service';
import {Location} from '@angular/common';
import {Recipes} from '../../model/recipes/recipes';

@Injectable({
    providedIn: 'root'
})

export class ExtrasService {

    private _recipe: Recipes;
    private _recipe_edit_type: string;


    constructor(public popCtrl: PopoverController, public authenticationProvider: AuthenticationService, public databaseProvider: DatabaseService, private location: Location) {
    }

    async showPopup(event, params, translucent) {
        const popover = await this.popCtrl.create({
            component: PopupsPage,
            componentProps: params,
            event: event,
            translucent: translucent
        });
        if (popover) {
            popover.present();
        }
        return popover;
    }

    initializeUser(self) {
        this.authenticationProvider.getUser().then((authUser) => {
            if (authUser) {
                const uid = authUser['uid'];
                this.databaseProvider.getSpecificUserDetails(uid).on('value', (snapshot) => {
                    const user = snapshot.val()[uid];
                    self.user = user as Users;
                });
            }
        }).catch(function (err) {
            console.log(err);
        });
    }

    setRating(rating, value) {
        if (rating === 1 && value === 1) {
            return 0;
        } else {
            return value;
        }
    }

    goBack() {
        this.location.back();
    }

    get recipe(): Recipes {
        return this._recipe;
    }

    set recipe(value: Recipes) {
        this._recipe = value;
    }

    get recipe_edit_type(): string {
        return this._recipe_edit_type;
    }

    set recipe_edit_type(value: string) {
        this._recipe_edit_type = value;
    }
}

