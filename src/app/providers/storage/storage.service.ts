import {Injectable} from '@angular/core';
import firebase from 'firebase';

@Injectable({
    providedIn: 'root'
})
export class StorageService {

    constructor() {
    }


    saveImages(file, extension) {
        return new Promise((resolve) => {
            const ref = firebase.storage().ref().child(`${Date.now().toString()}.${extension}`);
            ref.put(file).then(function(snapshot) {
                console.log('Uploaded a blob or file!');
                console.log(snapshot);
                resolve(ref.getDownloadURL());
            });
        });
    }
}
