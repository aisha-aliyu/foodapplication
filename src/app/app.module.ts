import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {RouteReuseStrategy} from '@angular/router';

import {IonicModule, IonicRouteStrategy, NavParams} from '@ionic/angular';
import {SplashScreen} from '@ionic-native/splash-screen/ngx';
import {StatusBar} from '@ionic-native/status-bar/ngx';

import {AppComponent} from './app.component';
import {AppRoutingModule} from './app-routing.module';
import {CookieService} from 'ngx-cookie-service';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import {
    MatButtonModule,
    MatCheckboxModule,
    MatPaginatorModule,
    MatSortModule,
    MatTableModule,
    MatTabsModule
} from '@angular/material';

import {ScrollingModule} from '@angular/cdk/scrolling';
import {PopupsPage} from './partials/popups/popups.page';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NavbarPage} from './partials/navbar/navbar.page';
import {NavbarPageModule} from './partials/navbar/navbar.module';
import {HomePageModule} from './pages/home/home.module';
import {AllrecipesPageModule} from './pages/allrecipes/allrecipes.module';


@NgModule({
    declarations: [AppComponent, PopupsPage],
    entryComponents: [PopupsPage],
    imports: [BrowserModule,
        IonicModule.forRoot(),
        AppRoutingModule,
        BrowserAnimationsModule,
        ScrollingModule,
        FormsModule,
        MatButtonModule, MatCheckboxModule,
        ReactiveFormsModule,
        MatTabsModule,
        MatTableModule,
        MatPaginatorModule,
        MatSortModule,
        MatCheckboxModule,
        MatTableModule,
        HomePageModule,
        AllrecipesPageModule
    ],
    providers: [
        StatusBar,
        SplashScreen,
        CookieService,
        {provide: RouteReuseStrategy, useClass: IonicRouteStrategy}
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
