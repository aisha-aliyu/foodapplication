import {Component} from '@angular/core';
import {RecipeElement} from '../../model/recipeElement/recipeElement';
import {MatTableDataSource,MatPaginator} from '@angular/material';
import {OnInit,ViewChild} from '@angular/core';
import {Users} from '../../model/users/users';
import {AuthenticationService} from '../../providers/authentication/authentication.service';
import {Router} from '@angular/router';
import {ExtrasService} from '../../providers/extras/extras.service';
import {Recipes} from '../../model/recipes/recipes';
import {DatabaseService} from '../../providers/database/database.service';
import { NumberSymbol } from '@angular/common';


@Component({
    selector: 'app-home',
    templateUrl: 'home.page.html',
    styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {

    section: string;
    user: Users;
    recipes: Recipes[] = [];
    public allRole = ['Admin', 'User'];
    user_list: Users[] = []
    role_list;
    adminlength;
    



    displayedRecipeColumns = ['name', 'category', 'numOfViews', 'options'];
    ELEMENT_RECIPE_DATA: Recipes[] = [];

    displayedColumns = ['email', 'firstName', 'lastName', 'admin'];
    ELEMENT_DATA: Users[] = [];

    // dataSource = this.ELEMENT_DATA;
    dataRecipeSource: MatTableDataSource<Recipes>;
    segmentValue = 'recipeManagement';
    dataUserSource: MatTableDataSource<Users>;

    @ViewChild('paginator') paginator: MatPaginator;

    constructor(public authenticationProvider: AuthenticationService, public extraService: ExtrasService, private router: Router, public databaseService: DatabaseService,) {

        this.user = this.authenticationProvider.user;
        setTimeout(() => {
            this.extraService.initializeUser(this);
        });

    }

    ngOnInit() {
        this.getAllRecipes();
        this.getAllUsers();

    }

    setTable() {
        this.dataRecipeSource = new MatTableDataSource<Recipes>(this.ELEMENT_RECIPE_DATA);
          this.dataUserSource = new MatTableDataSource<Users>(this.ELEMENT_DATA);
    }

    navigate(path) {
        this.router.navigateByUrl(path);

    }

    segmentChanged(ev: any) {
        this.segmentValue = ev.detail.value;
        // console.log(this.segmentValue);
    }


    getAllRecipes() {
        this.databaseService.getAllRecipes().on('value', (value) => {
            const allrecipes = value.val();
            const recipeList = [];
            for (let recipe in allrecipes) {
                recipeList.push(allrecipes[recipe]);
            }
            if (recipeList.length >5){
                recipeList.length = 5
            }
            this.recipes = recipeList;
            this.ELEMENT_RECIPE_DATA = recipeList;

            this.setTable();
        });
    }

  
    getAllUsers() {
        this.databaseService.getUserDetails().on('value', (snapshot) => {
            const users = snapshot.val();
            let userslist = [];
            for (let key in users) {
                if (users.hasOwnProperty(key)) {
                    userslist.push(users[key]);
                }
            }
            if (userslist.length >5){
                userslist.length = 5
            }
             this.user_list = userslist;
             this.ELEMENT_DATA = userslist;
             this.setTable(); 

            let i;
            const role_list = [];
            const admin_list =[];
            for (i = 0; i < this.user_list.length; i++ ) {
                role_list.push(this.user_list[i].role);
            }
           
            for (i = 0; i < role_list.length; i++ ) {
                if(role_list[i] === "admin"){
                  admin_list.push(role_list[i])
                }
            }
            this.adminlength = admin_list.length;

        });
    }

    viewRecipe(item) {
        this.extraService.recipe = item;
        this.extraService.recipe_edit_type = 'view';
        this.router.navigateByUrl('/addrecipes');
    }

    editRecipe(item) {
        this.extraService.recipe = item;
        this.extraService.recipe_edit_type = 'edit';
        this.router.navigateByUrl('/addrecipes');
    }

    deleteRecipe(item) {
        this.databaseService.deleteRecipe(item);
    }

    deleteUser(uid) {
        this.databaseService.deleteUser(uid);
    }
}




