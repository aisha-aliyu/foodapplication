import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {IonicModule, NavParams} from '@ionic/angular';
import {FormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';
import {MatTabsModule} from '@angular/material';
import {MatTableModule} from '@angular/material/table';
import {MatIconModule} from '@angular/material/icon';
import {MatCheckboxModule} from '@angular/material/checkbox';


import {HomePage} from './home.page';
import {NavbarPageModule} from '../../partials/navbar/navbar.module';


@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        MatTabsModule,
        MatTableModule,
        MatIconModule,
        MatCheckboxModule,
        RouterModule.forChild([
            {
                path: '',
                component: HomePage
            }
        ]),
        NavbarPageModule
    ],
    declarations: [HomePage],
    exports: [HomePage]
})

export class HomePageModule {
}

