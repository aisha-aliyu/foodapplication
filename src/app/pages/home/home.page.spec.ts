import {CUSTOM_ELEMENTS_SCHEMA, DebugElement} from '@angular/core';
import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {HomePage} from './home.page';
import {FirebaseMock} from '../../mocks/firebasemock';


import * as firebase from 'firebase/app';
import {CookieService} from 'ngx-cookie-service';
import {CookieServiceMock, LocationMock, PopupMock} from '../../mocks/others';
import {By} from '@angular/platform-browser';
import {Router} from '@angular/router';
import {
    MatCheckboxModule,
    MatPaginatorModule,
    MatSortModule,
    MatTableDataSource,
    MatTableModule,
    MatTabsModule
} from '@angular/material';
import {RouterTestingModule} from '@angular/router/testing';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {Recipes} from '../../model/recipes/recipes';
import {RecipeElement} from '../../model/recipeElement/recipeElement';
import * as RecipeTable from '../../mocks/recipeTable.json';
import {Location} from '@angular/common';
import {PopoverController} from '@ionic/angular';
import {Users} from '../../model/users/users';
import * as UserTable from '../../mocks/userTable.json';


describe('HomePage without user information', () => {
    let component: HomePage;
    let fixture: ComponentFixture<HomePage>;

    const mockFirebase: FirebaseMock = new FirebaseMock();
    let firebaseConnect;
    let firebaseAuth;

    beforeAll(() => {
        firebaseConnect = spyOn(firebase, 'database').and.returnValue(mockFirebase.database());
        firebaseAuth = spyOn(firebase, 'auth').and.returnValue(mockFirebase.auth());
    });

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [HomePage],
            schemas: [CUSTOM_ELEMENTS_SCHEMA],
            imports: [MatTableModule, RouterTestingModule, FormsModule, ReactiveFormsModule,
                BrowserAnimationsModule],
            providers: [
                {provide: CookieService, useClass: CookieServiceMock}, {
                    provide: PopoverController,
                    useClass: PopupMock
                },
            ],
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(HomePage);
        component = fixture.componentInstance;
        mockFirebase.userInfo = null;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should not set user information if value is true', () => {
        expect(firebaseAuth).toHaveBeenCalled();
        expect(component.user).toBeUndefined();
    });
});

describe('HomePage with user information', () => {
    let component: HomePage;
    let fixture: ComponentFixture<HomePage>;
    let location: Location;

    const mockRecipes: Recipes[] = RecipeTable.recipe as Recipes[];
    const mockUsers: Users[] = UserTable.user as Users[];

    const mockFirebase: FirebaseMock = new FirebaseMock();
    let firebaseConnect;
    let firebaseAuth;
    let debugElement: DebugElement;
    const mockNavControl: LocationMock = new LocationMock();

    beforeAll(() => {
        mockFirebase.userInfo = mockFirebase.user;
        firebaseConnect = spyOn(firebase, 'database').and.returnValue(mockFirebase.database());
        firebaseAuth = spyOn(firebase, 'auth').and.returnValue(mockFirebase.auth());
    });

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [HomePage],
            schemas: [CUSTOM_ELEMENTS_SCHEMA],
            imports: [MatTableModule, RouterTestingModule, FormsModule, ReactiveFormsModule,
                BrowserAnimationsModule],
            providers: [
                {provide: CookieService, useClass: CookieServiceMock}, {
                    provide: PopoverController,
                    useClass: PopupMock
                },
            ],
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(HomePage);
        component = fixture.componentInstance;
        fixture.detectChanges();
        debugElement = fixture.debugElement.query(By.css('app-navbar'));

        // // get the location module
        location = TestBed.get(Location);
        mockNavControl.location = location;
        fixture.ngZone.run(() => {
            location.go('');
        });
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should set user information if value is true ', () => {
        fixture.detectChanges();
        // setTimeout(() => {
        //     expect(component.user).toEqual(mockFirebase.userInfo);
        //     expect(debugElement).toBeTruthy();
        // }, 3000);
    });


    it('should test the recipe table row', async(() => {
        fixture.detectChanges();
        const tableRows = fixture.nativeElement.querySelectorAll('tr');
        expect(tableRows.length).toBe(2);
    }));

    it('should test the recipe table cell', async(() => {
        const tableRows = fixture.nativeElement.querySelectorAll('th');
        expect(tableRows.length).toBe(4);
    }));

    it('should test the recipe table header row', async(() => {
        const tableRows = fixture.nativeElement.querySelectorAll('tr');
        const headerRow = tableRows[0];
        expect(headerRow.cells[0].innerHTML).toBe(' NAME');
        expect(headerRow.cells[1].innerHTML).toBe(' CATEGORY');
        expect(headerRow.cells[2].innerHTML).toBe(' NUMBER OF VIEWS');
        expect(headerRow.cells[3].innerHTML).toBe(' OPTIONS');
    }));


    it('should test recipe table have data', () => {
        component.dataRecipeSource = new MatTableDataSource<Recipes>(mockRecipes);
        fixture.detectChanges();
        fixture.whenStable().then(() => {
            const tableRows = fixture.nativeElement.querySelectorAll('tr');
            expect(tableRows.length).toBe(1);
            const headerRow = tableRows[1];
            const options = headerRow.querySelectorAll('ion-icon');
            expect(headerRow.cells[1].innerHTML.trim()).toEqual(RecipeTable.recipe[0].name);
            expect(headerRow.cells[4].innerHTML.trim()).toEqual(RecipeTable.recipe[0].category);
            expect(headerRow.cells[6].innerHTML.trim()).toEqual(RecipeTable.recipe[0].noOfViews.toString());
            expect(options.length).toEqual(3);
        });
    });

    it('should go to view all page', () => {
        location.go('/allrecipes');
        component.navigate('allrecipes');
        expect(location.path()).toBe('/allrecipes');
    });

    it('should change tab test user management table row', async(() => {
        component.segmentValue = 'userManagement';
        fixture.detectChanges();
        const tableRows = fixture.nativeElement.querySelectorAll('tr');
        expect(tableRows.length).toBe(1);
    }));

    it('should change tab test user management table cell', async(() => {
        component.segmentValue = 'userManagement';
        fixture.detectChanges();
        const tableRows = fixture.nativeElement.querySelectorAll('th');
        expect(tableRows.length).toBe(4);
    }));

    it('should change tab test user management table header row', () => {

        component.segmentValue = 'userManagement';
        fixture.detectChanges();

        fixture.whenStable().then(() => {
            const tableRows = fixture.nativeElement.querySelectorAll('tr');
            const headerRow = tableRows[0];
            expect(headerRow.cells[0].innerText).toEqual('EMAIL');
            expect(headerRow.cells[1].innerText).toBe('FIRST NAME');
            expect(headerRow.cells[2].innerText).toBe('LAST NAME');
            expect(headerRow.cells[3].innerText).toBe('ADMIN');
        });
    });


    it('should change tab test user management table have data', () => {
        component.segmentValue = 'userManagement';
        fixture.detectChanges();
        component.dataUserSource = new MatTableDataSource<Users>(mockUsers);
        fixture.whenStable().then(() => {
            const tableRows = fixture.nativeElement.querySelectorAll('tr');
            expect(tableRows.length).toBe(1);
            const headerRow = tableRows[1];
            const options = headerRow.querySelectorAll('ion-icon');
            expect(headerRow.cells[1].innerHTML.trim()).toEqual(UserTable.user[0].email);
            expect(headerRow.cells[2].innerHTML.trim()).toEqual(UserTable.user[0].firstName);
            expect(headerRow.cells[3].innerHTML.trim()).toEqual(UserTable.user[0].lastName);
            expect(headerRow.cells[6].innerHTML.trim()).toEqual(UserTable.user[0].role);
        });
    });


});









