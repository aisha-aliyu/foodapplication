import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {Routes, RouterModule} from '@angular/router';
import {MatTabsModule} from '@angular/material';
import {MatTableModule, MatCheckboxModule, MatPaginatorModule, MatSortModule} from '@angular/material';

import {IonicModule} from '@ionic/angular';

import {AllrecipesPage} from './allrecipes.page';
import {NavbarPageModule} from '../../partials/navbar/navbar.module';

const routes: Routes = [
    {
        path: '',
        component: AllrecipesPage
    }
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        MatTabsModule,
        MatTableModule,
        MatPaginatorModule,
        MatSortModule,
        MatCheckboxModule,
        RouterModule.forChild(routes),
        NavbarPageModule
    ],
    declarations: [AllrecipesPage],
    exports: [AllrecipesPage]
})
export class AllrecipesPageModule {
}
