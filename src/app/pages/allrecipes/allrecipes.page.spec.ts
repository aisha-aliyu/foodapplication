import {CUSTOM_ELEMENTS_SCHEMA, DebugElement} from '@angular/core';
import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AllrecipesPage} from './allrecipes.page';
import {RouterTestingModule} from '@angular/router/testing';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatCheckboxModule, MatPaginatorModule, MatSortModule, MatTableModule, MatTabsModule} from '@angular/material';
import {MatTableDataSource, MatSort, MatPaginator} from '@angular/material';
import {CookieServiceMock, PopupMock} from '../../mocks/others';
import {CookieService} from 'ngx-cookie-service';
import {FirebaseMock} from '../../mocks/firebasemock';
import * as firebase from 'firebase/app';
import {Recipes} from '../../model/recipes/recipes';
import * as AllRecipesMock from '../../mocks/allRecipesMocks.json';
import {IonicModule, PopoverController} from '@ionic/angular';


describe('AllrecipesPage with no data', () => {
    let component: AllrecipesPage;
    let fixture: ComponentFixture<AllrecipesPage>;
    const mockFirebase: FirebaseMock = new FirebaseMock();
    let mockRecipes: Recipes[] = [];
    let firebaseConnect;
    let firebaseAuth;

    beforeAll(() => {
        firebaseConnect = spyOn(firebase, 'database').and.returnValue(mockFirebase.recipeDatabase());
        firebaseAuth = spyOn(firebase, 'auth').and.returnValue(mockFirebase.auth());
    });

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [AllrecipesPage],
            imports: [MatTableModule, MatPaginatorModule, MatSortModule, RouterTestingModule,
                MatSortModule, MatCheckboxModule, FormsModule, ReactiveFormsModule,
                BrowserAnimationsModule, MatTabsModule],
            schemas: [CUSTOM_ELEMENTS_SCHEMA],
            providers: [
                {provide: CookieService, useClass: CookieServiceMock}, {provide: PopoverController, useClass: PopupMock}
            ],
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(AllrecipesPage);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should have no data', () => {
        expect(component).toBeTruthy();
        component.recipeDataSource = new MatTableDataSource<Recipes>(mockRecipes);
        fixture.detectChanges();
        let tableRows = fixture.nativeElement.querySelector('#recp').querySelectorAll('tr');
        expect(tableRows.length).toBe(1);
        let headerRow = tableRows[0];
        expect(headerRow.cells[1].innerText).toEqual('Name');
        expect(headerRow.cells[2].innerText).toBe('Category');
        expect(headerRow.cells[3].innerText).toBe('No Of Views');
        expect(headerRow.cells[4].innerText).toBe('Options');
    });

});

describe('AllrecipesPage with data', () => {
    let component: AllrecipesPage;
    let fixture: ComponentFixture<AllrecipesPage>;

    let mockRecipes: Recipes[] = AllRecipesMock.recipes as Recipes[];
    const mockFirebase: FirebaseMock = new FirebaseMock();
    let firebaseConnect;
    let firebaseAuth;

    beforeAll(() => {
        firebaseConnect = spyOn(firebase, 'database').and.returnValue(mockFirebase.recipeDatabase());
        firebaseAuth = spyOn(firebase, 'auth').and.returnValue(mockFirebase.auth());
    });

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [AllrecipesPage],
            imports: [MatTableModule, MatPaginatorModule, MatSortModule, RouterTestingModule,
                MatSortModule, MatCheckboxModule, FormsModule, ReactiveFormsModule,
                BrowserAnimationsModule, MatTabsModule, IonicModule],
            schemas: [CUSTOM_ELEMENTS_SCHEMA],
            providers: [
                {provide: CookieService, useClass: CookieServiceMock}, {provide: PopoverController, useClass: PopupMock}
            ],
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(AllrecipesPage);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should have data', () => {
        component.recipeDataSource = new MatTableDataSource<Recipes>(mockRecipes);
        fixture.detectChanges();
        let tableRows = fixture.nativeElement.querySelector('#recp').querySelectorAll('tr');
        expect(tableRows.length).toBe(9);
        let headerRow = tableRows[1];
        let options = headerRow.querySelectorAll('ion-icon');
        expect(headerRow.cells[1].innerHTML.trim()).toEqual(AllRecipesMock.recipes[0].name);
        expect(headerRow.cells[2].innerHTML.trim()).toEqual(AllRecipesMock.recipes[0].category);
        expect(headerRow.cells[3].innerHTML.trim()).toEqual(AllRecipesMock.recipes[0].noOfViews.toString());
        expect(options.length).toEqual(3);
    });

});

describe('AllrecipesPage switch tabs', () => {
    let component: AllrecipesPage;
    let fixture: ComponentFixture<AllrecipesPage>;
    const mockFirebase: FirebaseMock = new FirebaseMock();
    let firebaseConnect;
    let firebaseAuth;

    beforeAll(() => {
        firebaseConnect = spyOn(firebase, 'database').and.returnValue(mockFirebase.recipeDatabase());
        firebaseAuth = spyOn(firebase, 'auth').and.returnValue(mockFirebase.auth());
    });

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [AllrecipesPage],
            imports: [MatTableModule, MatPaginatorModule, MatSortModule, RouterTestingModule,
                MatSortModule, MatCheckboxModule, FormsModule, ReactiveFormsModule,
                BrowserAnimationsModule, MatTabsModule],
            schemas: [CUSTOM_ELEMENTS_SCHEMA],
            providers: [
                {provide: CookieService, useClass: CookieServiceMock}, {provide: PopoverController, useClass: PopupMock}
            ],
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(AllrecipesPage);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should change tab', () => {
        expect(component).toBeTruthy();
        let tab2 = fixture.nativeElement.querySelectorAll('.mat-tab-label');
        let selectedtab = fixture.nativeElement.querySelectorAll('.mat-tab-label[tabindex="0"]');
        expect(selectedtab[0].innerText).toEqual('Recipe');
        tab2[1].click();
        fixture.detectChanges();
        selectedtab = fixture.nativeElement.querySelectorAll('.mat-tab-label[tabindex="0"]');
        expect(selectedtab[0].innerText).toEqual('Suggested');
    });


});


describe('AllrecipesPage Suggested tabs', () => {
    let component: AllrecipesPage;
    let fixture: ComponentFixture<AllrecipesPage>;
    let mockRecipes: Recipes[] = [];
    const mockFirebase: FirebaseMock = new FirebaseMock();
    let firebaseConnect;
    let firebaseAuth;

    beforeAll(() => {
        firebaseConnect = spyOn(firebase, 'database').and.returnValue(mockFirebase.recipeDatabase());
        firebaseAuth = spyOn(firebase, 'auth').and.returnValue(mockFirebase.auth());
    });

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [AllrecipesPage],
            imports: [MatTableModule, MatPaginatorModule, MatSortModule, RouterTestingModule,
                MatSortModule, MatCheckboxModule, FormsModule, ReactiveFormsModule,
                BrowserAnimationsModule, MatTabsModule],
            schemas: [CUSTOM_ELEMENTS_SCHEMA],
            providers: [
                {provide: CookieService, useClass: CookieServiceMock}, {provide: PopoverController, useClass: PopupMock}
            ],
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(AllrecipesPage);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should load new tab table with no data', () => {
        component.suggestedDataSource = new MatTableDataSource<Recipes>(mockRecipes);
        const tab2 = fixture.nativeElement.querySelectorAll('.mat-tab-label');
        tab2[1].click();
        fixture.detectChanges();
        let selectedtab = fixture.nativeElement.querySelectorAll('.mat-tab-label[tabindex="0"]');

        expect(selectedtab[0].innerText).toEqual('Suggested');

        fixture.whenStable().then(() => {
            expect(fixture.nativeElement.querySelector('#sugg')).toBeTruthy();
            let tableRows = fixture.nativeElement.querySelector('#sugg').querySelectorAll('tr');
            fixture.detectChanges();
            expect(tableRows.length).toBe(1);
            let headerRow = tableRows[0];
            expect(headerRow.cells[1].innerText).toEqual('Name');
            expect(headerRow.cells[2].innerText).toBe('Category');
            expect(headerRow.cells[3].innerText).toBe('No Of Views');
            expect(headerRow.cells[4].innerText).toBe('Options');
        });
    });

});


describe('AllrecipesPage Suggested tabs', () => {
    let component: AllrecipesPage;
    let fixture: ComponentFixture<AllrecipesPage>;
    let mockRecipes: Recipes[] = AllRecipesMock.recipes as Recipes[];
    const mockFirebase: FirebaseMock = new FirebaseMock();
    let firebaseConnect;
    let firebaseAuth;

    beforeAll(() => {
        firebaseConnect = spyOn(firebase, 'database').and.returnValue(mockFirebase.recipeDatabase());
        firebaseAuth = spyOn(firebase, 'auth').and.returnValue(mockFirebase.auth());
    });

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [AllrecipesPage],
            imports: [MatTableModule, MatPaginatorModule, MatSortModule, RouterTestingModule,
                MatSortModule, MatCheckboxModule, FormsModule, ReactiveFormsModule,
                BrowserAnimationsModule, MatTabsModule],
            schemas: [CUSTOM_ELEMENTS_SCHEMA],
            providers: [
                {provide: CookieService, useClass: CookieServiceMock}, {provide: PopoverController, useClass: PopupMock}
            ],
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(AllrecipesPage);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should load new tab table with data', (done) => {
        mockRecipes = mockRecipes.filter(function (item) {
            if (item['suggested'] === true) {
                return true;
            }
        });
        component.suggestedDataSource = new MatTableDataSource<Recipes>(mockRecipes);
        const tab2 = fixture.nativeElement.querySelectorAll('.mat-tab-label');
        tab2[1].click();
        fixture.detectChanges();
        let selectedtab = fixture.nativeElement.querySelectorAll('.mat-tab-label[tabindex="0"]');
        expect(selectedtab[0].innerText).toEqual('Suggested');
        fixture.detectChanges();
        fixture.whenStable().then(() => {
            fixture.detectChanges();
            let tableRows = fixture.nativeElement.querySelector('#sugg').querySelectorAll('tr');
            expect(tableRows.length).toBe(5);
            let firstRow = tableRows[1];
            let options = firstRow.querySelectorAll('ion-icon');
            expect(firstRow.cells[1].innerText.trim()).toEqual(AllRecipesMock.recipes[0].name);
            expect(firstRow.cells[2].innerText.trim()).toEqual(AllRecipesMock.recipes[0].category);
            expect(firstRow.cells[3].innerText.trim()).toEqual(AllRecipesMock.recipes[0].noOfViews.toString());
            expect(options.length).toEqual(3);
            done();
        });

    }, 500000);

});


describe('AllrecipesPage filter', () => {
    let component: AllrecipesPage;
    let fixture: ComponentFixture<AllrecipesPage>;
    const mockFirebase: FirebaseMock = new FirebaseMock();
    let firebaseConnect;
    let firebaseAuth;

    beforeAll(() => {
        firebaseConnect = spyOn(firebase, 'database').and.returnValue(mockFirebase.recipeDatabase());
        firebaseAuth = spyOn(firebase, 'auth').and.returnValue(mockFirebase.auth());
    });

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [AllrecipesPage],
            imports: [MatTableModule, MatPaginatorModule, MatSortModule, RouterTestingModule,
                MatSortModule, MatCheckboxModule, FormsModule, ReactiveFormsModule,
                BrowserAnimationsModule, MatTabsModule],
            schemas: [CUSTOM_ELEMENTS_SCHEMA],
            providers: [
                {provide: CookieService, useClass: CookieServiceMock}, {provide: PopoverController, useClass: PopupMock}
            ],
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(AllrecipesPage);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should hide filter', () => {
        expect(component).toBeTruthy();
        expect(component.hideMe).toBeFalsy();
        let filterbutton = fixture.nativeElement.querySelectorAll('#filter');
        filterbutton[0].click();
        fixture.detectChanges();
        expect(component.hideMe).toBeTruthy();
    });

});

