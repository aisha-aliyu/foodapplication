import {Component, OnInit, ViewChild, AfterViewInit} from '@angular/core';
import {Recipes} from '../../model/recipes/recipes';
import {SelectionModel} from '@angular/cdk/collections';
import {MatTableDataSource, MatSort, MatPaginator} from '@angular/material';
import {Users} from '../../model/users/users';
import {AuthenticationService} from '../../providers/authentication/authentication.service';
import {DatabaseService} from '../../providers/database/database.service';
import {ExtrasService} from '../../providers/extras/extras.service';
import {Router} from '@angular/router';

@Component({
    selector: 'app-allrecipes',
    templateUrl: './allrecipes.page.html',
    styleUrls: ['./allrecipes.page.scss'],
})


export class AllrecipesPage implements OnInit {
    displayedColumns: string[] = ['select', 'name', 'category', 'noOfViews', 'options'];
    suggestedDisplayedColumns: string[] = ['select', 'name', 'category', 'noOfViews', 'options'];
    hideMe = false;
    user: Users;
    recipes: Recipes[] = [];
    categories = [];
    countries = [];
    newrating = 0;
    difficulty = '';
    selectedCategory = '';
    newpreptime;
    newtime;
    selectedCountry = '';
    time = {upper: 0, lower: 0};
    preptime = {upper: 0, lower: 0};
    recipeDataSource: MatTableDataSource<Recipes>;
    suggestedDataSource: MatTableDataSource<Recipes>;
    ELEMENT_DATA: Recipes[] = [];
    ELEMENT_DATA_SUGGESTED: Recipes[] = [];

    selection = new SelectionModel<Recipes>(true, []);
    selectionSuggested = new SelectionModel<Recipes>(true, []);

    @ViewChild('paginator') paginator: MatPaginator;
    @ViewChild('suggestedPaginator') suggestedPaginator: MatPaginator;

    @ViewChild('recipeSort') recipeSort: MatSort;
    @ViewChild('suggestedSort') suggestedSort: MatSort;

    constructor(public authenticationProvider: AuthenticationService, public extraService: ExtrasService,
                public databaseService: DatabaseService, private router: Router) {
        this.user = this.authenticationProvider.user;
        setTimeout(() => {
            this.extraService.initializeUser(this);
        });
    }

    ngOnInit() {
        this.getAllRecipes();
    }

    setTables() {
        this.recipeDataSource = new MatTableDataSource<Recipes>(this.ELEMENT_DATA);
        this.suggestedDataSource = new MatTableDataSource<Recipes>(this.ELEMENT_DATA_SUGGESTED);
        this.recipeDataSource.sort = this.recipeSort;
        this.recipeDataSource.paginator = this.paginator;
        this.suggestedDataSource.sort = this.suggestedSort;
        this.suggestedDataSource.paginator = this.suggestedPaginator;
    }

    setFilters(recipes) {
        const prepTime = [];
        const cookTime = [];
        for (let recipe in recipes) {
            this.categories.push(recipes[recipe].category);
            this.countries.push(recipes[recipe].country);
            prepTime.push(recipes[recipe].preparation.value);
            cookTime.push(recipes[recipe].cooking.value);
        }
        this.categories = this.categories.filter((v, i) => this.categories.indexOf(v) === i);
        this.countries = this.countries.filter((v, i) => this.countries.indexOf(v) === i);
        this.preptime.upper = Math.max(...prepTime) as number;
        this.preptime.lower = Math.min(...prepTime) as number;
        this.time.upper = Math.max(...cookTime) as number;
        this.time.lower = Math.min(...cookTime) as number;
        this.newpreptime = this.preptime;
        this.newtime = this.time;
    }

    isAllSuggestedSelected() {
        const numSelected = this.selectionSuggested.selected.length;
        const numRows = this.suggestedDataSource.data.length;
        return numSelected === numRows;
    }

    /** Selects all rows if they are not all selected; otherwise clear selection. */
    masterSuggestedToggle() {
        this.isAllSuggestedSelected() ?
            this.selectionSuggested.clear() :
            this.suggestedDataSource.data.forEach(row => this.selectionSuggested.select(row));
    }

    isAllRecipesSelected() {
        const numSelected = this.selection.selected.length;
        const numRows = this.recipeDataSource.data.length;
        return numSelected === numRows;
    }

    rating(value) {
        this.newrating = this.extraService.setRating(this.newrating, value);
    }

    segmentChanged(event) {
        this.difficulty = event.detail.value;
    }

    clear() {
        this.newrating = 0;
        this.selectedCategory = '';
        this.selectedCountry = '';
        this.difficulty = '';
        this.setFilters(this.recipes);
        this.ELEMENT_DATA = this.recipes as Recipes[];
        this.setTables();
    }

    navigate(path) {
        this.router.navigateByUrl(path);
    }

    /** Selects all rows if they are not all selected; otherwise clear selection. */
    masterRecipeToggle() {
        this.isAllRecipesSelected() ?
            this.selection.clear() :
            this.recipeDataSource.data.forEach(row => this.selection.select(row));
    }

    hide() {
        this.hideMe = this.hideMe === false ? true : false;
    }

    getAllRecipes() {
        this.databaseService.getAllRecipes().on('value', (snapshot) => {
            const allrecipes = snapshot.val();
            const recipeList = [];
            for (let recp in allrecipes) {
                recipeList.push(allrecipes[recp]);
            }
            this.recipes = recipeList;
            this.ELEMENT_DATA = recipeList;
            this.ELEMENT_DATA_SUGGESTED = recipeList.filter(function (item) {
                if (item['suggested'] === true) {
                    return true;
                }
            });
            this.setTables();
            this.setFilters(recipeList);
        });
    }

    viewRecipe(item) {
        this.extraService.recipe = item;
        this.extraService.recipe_edit_type = 'view';
        this.router.navigateByUrl('/addrecipes');
    }

    editRecipe(item) {
        this.extraService.recipe = item;
        this.extraService.recipe_edit_type = 'edit';
        this.router.navigateByUrl('/addrecipes');
    }

    deleteRecipe(item) {
        this.databaseService.deleteRecipe(item);
    }

    filter() {
        const filter = {
            country: this.selectedCountry !== '' ? this.selectedCountry : null,
            category: this.selectedCategory !== '' ? this.selectedCategory : null,
            difficulty: this.difficulty !== '' ? this.difficulty : null,
            spicy: this.newrating !== 0 ? this.newrating : null,
            preparation: this.newpreptime,
            cooking: this.newtime
        };
        const filterdList = this.recipes.filter(function (item) {
            let found = false;

            if ((filter['country'] == null || item['country'] === filter['country'])
                && (filter['category'] === null || item['category'] === filter['category'])
                && (filter['spicy'] === null || item['spicy'] === filter['spicy'])
                && (filter['difficulty'] === null || item['difficulty'] === filter['difficulty'])
                && (item['preparation']['value'] >= filter['preparation']['lower'] && item['preparation']['value'] <= filter['preparation']['upper'] )
                && (item['cooking']['value'] >= filter['cooking']['lower'] && item['cooking']['value'] <= filter['cooking']['upper'] )) {
                // return false;
                return true;

            }

        });
        this.ELEMENT_DATA = filterdList as Recipes[];
        this.setTables();
    }
}
