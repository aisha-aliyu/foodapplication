import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {LoginPage} from './login.page';
import {ReactiveFormsModule, FormsModule} from '@angular/forms';
import {RouterTestingModule} from '@angular/router/testing';
import {CookieServiceMock, LocationMock} from '../../mocks/others';
import {CookieService} from 'ngx-cookie-service';
import {Location} from '@angular/common';
import {AuthenticationService} from '../../providers/authentication/authentication.service';
import {By} from '@angular/platform-browser';

import {FirebaseMock} from '../../mocks/firebasemock';
import * as firebase from 'firebase/app';
import 'firebase/database';
import {NavController} from '@ionic/angular';

describe('LoginPage', () => {
    let service: AuthenticationService;
    let component: LoginPage;
    let fixture: ComponentFixture<LoginPage>;
    let location: Location;
    let mockFirebase: FirebaseMock = new FirebaseMock();
    let firebaseConnect;
    let firebaseAuth;
    let loginBtn: HTMLButtonElement;
    const mockNavControl: LocationMock = new LocationMock();

    beforeAll(() => {
        mockFirebase.userInfo = mockFirebase.user;
        firebaseConnect = spyOn(firebase, 'database').and.returnValue(mockFirebase.database());
        firebaseAuth = spyOn(firebase, 'auth').and.returnValue(mockFirebase.auth());
    });
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                ReactiveFormsModule, FormsModule, RouterTestingModule
            ],
            providers: [
                {provide: CookieService, useClass: CookieServiceMock},
                {provide: NavController, useValue: mockNavControl},
            ],
            declarations: [LoginPage],
            schemas: [CUSTOM_ELEMENTS_SCHEMA]
        }).compileComponents();
    }));

    beforeEach(() => {

        fixture = TestBed.createComponent(LoginPage);
        component = fixture.componentInstance;
        fixture.detectChanges();
        service = TestBed.get(AuthenticationService);


        // // get the location module
        location = TestBed.get(Location);
        mockNavControl.location = location;
        fixture.ngZone.run(() => {
            location.go('');
        });
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('User fail to log in ', () => {
        // get the location module
        location.go('/login');
        component.login();
        expect(component.userinfofailed).toBe(true);
        expect(location.path()).toBe('/login');

    });

    it('User email or password is not right', () => {
        component.userForm.controls['email'].setValue('test@gmail.com');
        component.userForm.controls['password'].setValue('123456789');
        component.userForm.controls['remember'].setValue('');
        component.login();

        setTimeout(() => {
            expect(component.userinfofailed).toBe(true);
            expect(firebaseAuth).toHaveBeenCalled();
            expect(component.error_message).toBe('There is no user record corresponding to this identifier. The user may have been deleted.');
        });
    });

    it('User log in successfully without cookie', () => {
        // get the location module
        component.userForm.controls['email'].setValue('test@gmail.com');
        component.userForm.controls['password'].setValue('rest');
        component.userForm.controls['remember'].setValue(false);
        component.login();
        setTimeout(() => {
            expect(firebaseAuth).toHaveBeenCalled();
            expect(firebaseConnect).toHaveBeenCalled();
            expect(location.path()).toBe('/');
            expect(component.userinfofailed).toBe(false);
            expect(component.userForm.value.remember).toBe(false);
            expect(service.checkCookie('remember')).toBe(false);
        });
    });

    it('User log in successfully with cookie', () => {
        // get the location module
        component.userForm.controls['email'].setValue('test@gmail.com');
        component.userForm.controls['password'].setValue('rest');
        component.userForm.controls['remember'].setValue(true);
        component.login();
        setTimeout(() => {
            expect(firebaseAuth).toHaveBeenCalled();
            expect(firebaseConnect).toHaveBeenCalled();
            expect(location.path()).toBe('/');
            expect(component.userinfofailed).toBe(false);
            expect(component.userForm.value.remember).toBe(true);
            expect(service.checkCookie('remember')).toBe(true);
        });
    });

    it('should disable login button if form is invalid', () => {
        loginBtn = fixture.debugElement.query(By.css('.loginBtn')).nativeElement;
        component.userForm.controls['email'].setValue('');
        component.userForm.controls['password'].setValue('');
        expect(component.userForm.valid).toBeFalsy();
        expect(loginBtn.disabled).toBe(true);
    });


    it('should go to terms and conditions page', () => {
        component.goToTerms();
        expect(location.path()).toBe('/terms');
    });

});







