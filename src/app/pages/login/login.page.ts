import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {AuthenticationService} from '../../providers/authentication/authentication.service';
import {FormBuilder, Validators} from '@angular/forms';
import {NavController} from '@ionic/angular';
import {Users} from '../../model/users/users';
import {DatabaseService} from '../../providers/database/database.service';


@Component({
    selector: 'app-login',
    templateUrl: './login.page.html',
    styleUrls: ['./login.page.scss']
})
export class LoginPage implements OnInit {
    public userForm;
    userinfofailed = false;
    error_message = '';

    constructor(
        private router: Router,
        private authenticationService: AuthenticationService,
        private databaseProvider: DatabaseService,
        private formBuilder: FormBuilder,
        private navController: NavController
    ) {
        this.userForm = formBuilder.group({
            email: [window.atob(this.authenticationService.getCookie('remember')), Validators.compose([Validators.required])],
            password: ['', Validators.compose([Validators.required])],
            remember: [this.authenticationService.checkCookie('remember')]
        });
    }

    ngOnInit() {
    }

    login() {
        if (this.userForm.valid) {
            this.authenticationService.verifyUser(this.userForm.value.email, this.userForm.value.password).then((result) => {
                this.databaseProvider.getSpecificUserDetails(result.user.uid).on('value', (snapshot) => {
                    const user = snapshot.val()[result.user.uid];
                    this.authenticationService.user = user as Users;
                    if (this.userForm.value.remember) {
                        this.authenticationService.setCookie('remember', window.btoa(this.userForm.value.email), 1000);
                    } else {
                        this.authenticationService.deleteCookie('remember');
                    }
                    this.router.navigateByUrl('');
                });
            }).catch(error => {
                console.log(error)
                this.userinfofailed = true;
                if (error.code) {
                    this.error_message = error.message;
                } else {
                    this.error_message = 'An error occurred on the server whilst trying to authenticate, please try again.';
                }
            });
        } else {
            this.userinfofailed = true;
            this.error_message = 'Please fill out all fields';
        }
    }

    resetPassword() {
        this.router.navigateByUrl('/forgot-password');
    }

    facebookLogin() {
        this.authenticationService.facebookLogin().then(result => {
            this.databaseProvider.getSpecificUserDetails(result.user.uid).on('value', (snapshot) => {
                const user = snapshot.val()[result.user.uid];
                this.authenticationService.user = user as Users;
                if (this.userForm.value.remember) {
                    this.authenticationService.setCookie('remember', window.btoa(this.userForm.value.email), 1000);
                } else {
                    this.authenticationService.deleteCookie('remember');
                }
                this.router.navigateByUrl('');
            });
        });
    }

    twitterlogin() {
        this.authenticationService.twitterLogin().then(result => {
            this.databaseProvider.getSpecificUserDetails(result.user.uid).on('value', (snapshot) => {
                const user = snapshot.val()[result.user.uid];
                this.authenticationService.user = user as Users;
                if (this.userForm.value.remember) {
                    this.authenticationService.setCookie('remember', window.btoa(this.userForm.value.email), 1000);
                } else {
                    this.authenticationService.deleteCookie('remember');
                }
                this.router.navigateByUrl('');
            });
        });
    }

    goolelogin() {
        this.authenticationService.googleLogin().then(result => {
            console.log(result);
        });
    }

    register() {
        this.router.navigateByUrl('/register');
    }

    close() {
        this.userinfofailed = false;
    }

    goToTerms() {
        this.navController.navigateForward('/terms');
    }
}
