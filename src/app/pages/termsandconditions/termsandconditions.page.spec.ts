import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {Location} from '@angular/common';

import {TermsandconditionsPage} from './termsandconditions.page';
import {RouterTestingModule} from '@angular/router/testing';

describe('TermsandconditionsPage', () => {
    let component: TermsandconditionsPage;
    let fixture: ComponentFixture<TermsandconditionsPage>;
    let location: Location;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [TermsandconditionsPage],
            imports: [RouterTestingModule],
            schemas: [CUSTOM_ELEMENTS_SCHEMA],
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(TermsandconditionsPage);
        component = fixture.componentInstance;
        fixture.detectChanges();

        // get the location module
        location = TestBed.get(Location);
        fixture.ngZone.run(() => {
            location.go('terms');
        });
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should go back to previous page', () => {
        expect(location.path()).toBe('/terms');
        component.goBack();
        expect(location.path()).toBe('');
    });
});
