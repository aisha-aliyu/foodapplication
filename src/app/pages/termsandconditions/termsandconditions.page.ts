import {Component, OnInit} from '@angular/core';
import {NavController} from '@ionic/angular';

@Component({
    selector: 'app-termsandconditions',
    templateUrl: './termsandconditions.page.html',
    styleUrls: ['./termsandconditions.page.scss'],
})
export class TermsandconditionsPage implements OnInit {

    constructor(private navController: NavController) {
    }

    ngOnInit() {
    }

    goBack() {
        this.navController.back();
    }
}
