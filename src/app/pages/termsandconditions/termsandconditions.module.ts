import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { TermsandconditionsPage } from './termsandconditions.page';

const routes: Routes = [
  {
    path: '',
    component: TermsandconditionsPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [TermsandconditionsPage]
})
export class TermsandconditionsPageModule {}
