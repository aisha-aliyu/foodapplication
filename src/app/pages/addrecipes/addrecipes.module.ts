import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {Routes, RouterModule} from '@angular/router';

import {IonicModule} from '@ionic/angular';

import {AddrecipesPage} from './addrecipes.page';
import {NavbarPageModule} from '../../partials/navbar/navbar.module';

const routes: Routes = [
    {
        path: '',
        component: AddrecipesPage
    }
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        IonicModule,
        RouterModule.forChild(routes),
        NavbarPageModule
    ],
    declarations: [AddrecipesPage]
})
export class AddrecipesPageModule {
}
