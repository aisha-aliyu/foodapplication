import {Component, OnInit, ViewChild} from '@angular/core';
import {AuthenticationService} from '../../providers/authentication/authentication.service';
import {Users} from '../../model/users/users';
import {FormBuilder, Validators} from '@angular/forms';
import {Measurements} from '../../model/recipes/measurements';
import {Ingredients} from '../../model/recipes/ingredients';
import {Directions} from '../../model/recipes/directions';
import {ExtrasService} from '../../providers/extras/extras.service';
import {StorageService} from '../../providers/storage/storage.service';
import {IonContent, LoadingController} from '@ionic/angular';
import {Recipes} from '../../model/recipes/recipes';
import {DatabaseService} from '../../providers/database/database.service';


@Component({
    selector: 'app-addrecipes',
    templateUrl: './addrecipes.page.html',
    styleUrls: ['./addrecipes.page.scss'],
})
export class AddrecipesPage implements OnInit {
    user: Users;
    public addForm;
    ingredients: Ingredients[] = [];
    directions: Directions[] = [];
    units = ['min(s)', 'hour(s)', 'second(s)'];
    categories = ['Appetizers', 'Soups and Salads', 'Vegetables', 'Main Dishes', 'Desserts'];
    files = [];
    drag_description = 'drag and drop your .png or .jpeg files here or click to upload';
    error = false;
    files_available = false;
    files_new = false;
    error_message = '';
    view_type = 'add';
    suggested = false;
    current_index;
    slideOpts = {
        initialSlide: 0,
        speed: 400
    };

    @ViewChild('pageTop') pageTop: IonContent;

    constructor(private formBuilder: FormBuilder, public authenticationProvider: AuthenticationService, public extraService: ExtrasService,
                public dataService: DatabaseService, public storageService: StorageService, public loadingCtrl: LoadingController) {
        this.user = this.authenticationProvider.user;
        this.view_type = this.extraService.recipe_edit_type ? this.extraService.recipe_edit_type : this.view_type;
        const recipe = this.view_type && (this.view_type === 'edit' || this.view_type === 'view') ? this.extraService.recipe : null;
        this.addForm = formBuilder.group({
            id: [recipe && recipe.id ? recipe.id as string : Date.now().toString() as string],
            name: [recipe && recipe.name ? recipe.name as string : '' as string,
                Validators.compose([Validators.required])],
            description: [recipe && recipe.description ? recipe.description as string : '' as string,
                Validators.compose([Validators.required])],
            country: [recipe && recipe.country ? recipe.country as string : '' as string,
                Validators.compose([Validators.required])],
            category: [recipe && recipe.category ? recipe.category as string : this.categories[0] as string,
                Validators.compose([Validators.required])],
            difficulty: [recipe && recipe.difficulty ? recipe.difficulty as string : 'easy' as string,
                Validators.compose([Validators.required])],
            spicy: [recipe && recipe.spicy ? recipe.spicy as number : 0 as number,
                Validators.compose([Validators.required])],
            preparationValue: [recipe && recipe.preparation &&
            recipe.preparation.value ? recipe.preparation.value as number : 0 as number, Validators.compose([Validators.required])],
            preparationUnit: [recipe && recipe.preparation &&
            recipe.preparation.units ? recipe.preparation.units as string : this.units[0] as string,
                Validators.compose([Validators.required])],
            cookingValue: [recipe && recipe.cooking && recipe.cooking.value ? recipe.cooking.value as number : 0 as number,
                Validators.compose([Validators.required])],
            cookingUnit: [recipe && recipe.cooking && recipe.cooking.units ? recipe.cooking.units as string : this.units[0] as string,
                Validators.compose([Validators.required])],
        });
        this.files_available = recipe && recipe.imageURLs && recipe.imageURLs.length > 0;
        this.drag_description = this.files_available ? recipe.imageURLs.join(',') : this.drag_description;
        this.files = this.files_available ? recipe.imageURLs : [];
        this.directions = recipe && recipe.directions && recipe.directions.length > 0 ? recipe.directions : [];
        this.ingredients = recipe && recipe.ingredients && recipe.ingredients.length > 0 ? recipe.ingredients : [];
        if (this.view_type === 'view') {
            this.addForm.disable();
        }
        setTimeout(() => {
            this.extraService.initializeUser(this);
        });
    }

    ngOnInit() {
    }

    setFiles(event) {
        const files = event.target.files;
        if (files && files.length > 0) {
            this.files = Array.from(files);
            const file_names = this.files.map((file) => {
                return file.name;
            });
            this.drag_description = file_names.join(',');
            this.files_available = true;
            this.files_new = true;
        }

    }

    async showPop(event, type, extras) {
        const popover = await this.extraService.showPopup(event, {type: type, extras: extras}, false);
        popover.onDidDismiss().then((data) => {
            if (data && data.data) {
                if (type === 'ingredients') {
                    this.ingredients.push(data.data);
                    console.log(data.data);
                    if (this.current_index) {
                        this.ingredients.splice(this.current_index, 1);
                        this.current_index = null;
                    }
                } else {
                    this.directions.push(data.data);
                    if (this.current_index) {
                        this.directions.splice(this.current_index, 1);
                        this.current_index = null;
                    }
                }

            }
        });

    }

    spiceLevel(value) {
        this.addForm.value.spicy = this.extraService.setRating(this.addForm.value.spicy, value);
    }

    uploadImage(file) {
        return new Promise((resolve) => {
            this.storageService.saveImages(file, file.name.split('.').pop()).then((result) => {
                resolve(result);
            }).catch((error) => {
            });
        });
    }

    async saveRecipe() {
        const loading = await this.loadingCtrl.create({
            message: 'Saving Recipe ...'
        });
        await loading.present();
        if (this.directions.length > 0 && this.ingredients.length > 0 && this.files.length > 0) {
            if (this.addForm.valid) {
                const promise_list = [];
                if (this.files_new) {
                    for (let i = 0; i < this.files.length; i++) {
                        promise_list.push(this.uploadImage(this.files[i]));
                    }
                }
                const results = this.files_new ? await Promise.all(promise_list) : this.files;
                const recipe = new Recipes(this.view_type && this.view_type === 'edit' ? this.addForm.value.id : Date.now().toString(), this.addForm.value.name, this.addForm.value.description,
                    this.addForm.value.country, this.addForm.value.category, this.user.uid, results as [string],
                    this.addForm.value.difficulty, this.addForm.value.spicy, {
                        value: this.addForm.value.preparationValue,
                        units: this.addForm.value.preparationUnit
                    } as Measurements, {
                        value: this.addForm.value.cookingValue,
                        units: this.addForm.value.cookingUnit
                    } as Measurements, this.ingredients as [Ingredients], this.directions as [Directions], this.suggested,
            );
                this.dataService.saveRecipe(recipe).then(() => {
                    this.extraService.goBack();

                });
                await loading.dismiss();
            } else {
                this.error = true;
                this.error_message = 'One or more fields have not been filled';
            }
        } else {
            this.error = true;
            this.error_message = this.directions.length < 1 ? 'You have not indicated any directions.'
                : this.ingredients.length < 1 ? 'You have not indicated any ingredients.' : 'You have not indicated any files.';
            await loading.dismiss();
            this.goToTop();
        }

    }

    goToTop() {
        this.pageTop.scrollToTop();
    }

    delete(index, type) {
        if (type === 'ingredients') {
            this.ingredients.splice(index, 1);
        } else {
            this.directions.splice(index, 1);
        }
    }

    async edit(index, event, type, object) {
        this.current_index = index;
        await this.showPop(event, type, object);
    }

    duplicate(type, object) {
        if (type === 'ingredients') {
            this.ingredients.push(object);
        } else {
            this.directions.push(object);
        }

    }

    cancel() {
        this.extraService.goBack();
    }
}
