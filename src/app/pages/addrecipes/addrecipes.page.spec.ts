import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {AddrecipesPage} from './addrecipes.page';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {RouterTestingModule} from '@angular/router/testing';
import {CookieServiceMock} from '../../mocks/others';
import {CookieService} from 'ngx-cookie-service';
import {PopoverController} from '@ionic/angular';
import {PopupMock} from '../../mocks/others';
import {FirebaseMock} from '../../mocks/firebasemock';
import * as firebase from 'firebase/app';
import {By} from '@angular/platform-browser';
import {Ingredients} from '../../model/recipes/ingredients';
import {DatabaseService} from '../../providers/database/database.service';

describe('AddrecipesPage', () => {
    let component: AddrecipesPage;
    let fixture: ComponentFixture<AddrecipesPage>;
    const mockFirebase: FirebaseMock = new FirebaseMock();
    let firebaseInitialize;
    let firebaseAuth;
    let firebaseConnect;
    let cancelMock;
    let uploadImageMock;
    let saveRecipeMock;
    let popMock;

    beforeAll(() => {
        firebaseInitialize = spyOn(firebase, 'initializeApp').and.returnValue(mockFirebase.initializeApp({}));
        firebaseAuth = spyOn(firebase, 'auth').and.returnValue(mockFirebase.auth());
        firebaseConnect = spyOn(firebase, 'database').and.returnValue(mockFirebase.database());
    });

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [AddrecipesPage],
            schemas: [CUSTOM_ELEMENTS_SCHEMA],
            providers: [{provide: CookieService, useClass: CookieServiceMock}, {provide: PopoverController, useClass: PopupMock}],
            imports: [
                ReactiveFormsModule, FormsModule, RouterTestingModule
            ],
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(AddrecipesPage);
        component = fixture.componentInstance;
        // Component variables.
        component.user = mockFirebase.user;
        component.directions = [{'value': 'Add Rice to Broth.'}];
        component.ingredients = [{'name': 'Rice', 'measurements': {'value': 1, 'units': 'cups'}}] as Ingredients[];
        component.files = ['file1'];
        // Mocks
        cancelMock = spyOn(component, 'cancel');
        uploadImageMock = spyOn(component, 'uploadImage');
        popMock = spyOn(component, 'showPop');
        saveRecipeMock = spyOn(DatabaseService.prototype, 'saveRecipe');
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    // Recipe Form testing
    it('should setup form section', () => {
        const formElement = fixture.debugElement.query(By.css('ion-content form'));
        const buttonElement = fixture.debugElement.query(By.css('#cancelForm'));
        const buttonElement2 = fixture.debugElement.query(By.css('#saveForm'));
        const imageElement = fixture.debugElement.nativeElement.querySelector('#imageSection');

        expect(formElement).toBeTruthy();
        expect(buttonElement).toBeTruthy();
        expect(buttonElement2).toBeTruthy();
        expect(imageElement).toBeFalsy();
    });

    it('should cancel form section', () => {
        const buttonElement = fixture.debugElement.nativeElement.querySelector('#cancelForm');
        buttonElement.click();
        fixture.whenStable().then(() => {
            expect(cancelMock).toHaveBeenCalled();
        });
    });

    it('should not enable save form if form is invalid', () => {
        component.addForm.controls['name'].setValue('');
        fixture.detectChanges();
        const buttonElement = fixture.debugElement.nativeElement.querySelector('#saveForm');
        expect(buttonElement.disabled).toBeTruthy();
        expect(component.addForm.valid).toBeFalsy();
    });

    it('should validate form but disable save due to invalid parameters(images/directions/ingredients are empty)', () => {
        component.addForm.controls['name'].setValue('Fried Rice');
        component.addForm.controls['description'].setValue('Cooked with Rice');
        component.addForm.controls['country'].setValue('Any');
        component.addForm.controls['category'].setValue('Main Dishes');
        component.addForm.controls['difficulty'].setValue('easy');
        component.addForm.controls['spicy'].setValue(1);
        component.addForm.controls['preparationValue'].setValue(10);
        component.addForm.controls['preparationUnit'].setValue('minute');
        component.addForm.controls['cookingValue'].setValue(20);
        component.addForm.controls['cookingUnit'].setValue('minute');
        component.directions = [];
        component.ingredients = [];
        component.files = [];


        fixture.detectChanges();
        const iconElement = fixture.debugElement.nativeElement.querySelector('#duplicateIngredients');
        const iconElement2 = fixture.debugElement.nativeElement.querySelector('#duplicateDirectionss');
        expect(iconElement).toBeFalsy();
        expect(iconElement2).toBeFalsy();
        expect(component.addForm.valid).toBeTruthy();
        const buttonElement = fixture.debugElement.nativeElement.querySelector('#saveForm');
        expect(buttonElement.disabled).toBeTruthy();
    });

    it('should validate form and enable save due to valid parameters(images/directions/ingredients are not empty)', () => {
        component.addForm.controls['name'].setValue('Fried Rice');
        component.addForm.controls['description'].setValue('Cooked with Rice');
        component.addForm.controls['country'].setValue('Any');
        component.addForm.controls['category'].setValue('Main Dishes');
        component.addForm.controls['difficulty'].setValue('easy');
        component.addForm.controls['spicy'].setValue(1);
        component.addForm.controls['preparationValue'].setValue(10);
        component.addForm.controls['preparationUnit'].setValue('minute');
        component.addForm.controls['cookingValue'].setValue(20);
        component.addForm.controls['cookingUnit'].setValue('minute');
        component.files_available = true;

        fixture.detectChanges();
        expect(component.addForm.valid).toBeTruthy();
        const buttonElement = fixture.debugElement.nativeElement.querySelector('#saveForm');
        expect(buttonElement.disabled).toBeFalsy();
    });

    it('should save Recipe', () => {
        component.addForm.controls['name'].setValue('Fried Rice');
        component.addForm.controls['description'].setValue('Cooked with Rice');
        component.addForm.controls['country'].setValue('Any');
        component.addForm.controls['category'].setValue('Main Dishes');
        component.addForm.controls['difficulty'].setValue('easy');
        component.addForm.controls['spicy'].setValue(1);
        component.addForm.controls['preparationValue'].setValue(10);
        component.addForm.controls['preparationUnit'].setValue('minute');
        component.addForm.controls['cookingValue'].setValue(20);
        component.addForm.controls['cookingUnit'].setValue('minute');
        component.files_available = true;
        fixture.detectChanges();
        expect(component.addForm.valid).toBeTruthy();
        const buttonElement = fixture.debugElement.nativeElement.querySelector('#saveForm');
        expect(buttonElement.disabled).toBeFalsy();
        buttonElement.click();
        fixture.detectChanges();
        fixture.whenStable().then(() => {
            expect(uploadImageMock).toHaveBeenCalledTimes(1);
            expect(saveRecipeMock).toHaveBeenCalled();
            expect(firebaseConnect).toHaveBeenCalled();
        });
    });
});

describe('AddrecipesPage Test Other Parts Of Code', () => {
    let component: AddrecipesPage;
    let fixture: ComponentFixture<AddrecipesPage>;
    const mockFirebase: FirebaseMock = new FirebaseMock();
    let firebaseInitialize;
    let firebaseAuth;
    let firebaseConnect;
    let popMock;

    beforeAll(() => {
        firebaseInitialize = spyOn(firebase, 'initializeApp').and.returnValue(mockFirebase.initializeApp({}));
        firebaseAuth = spyOn(firebase, 'auth').and.returnValue(mockFirebase.auth());
        firebaseConnect = spyOn(firebase, 'database').and.returnValue(mockFirebase.database());
    });

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [AddrecipesPage],
            schemas: [CUSTOM_ELEMENTS_SCHEMA],
            providers: [{provide: CookieService, useClass: CookieServiceMock}, {provide: PopoverController, useClass: PopupMock}],
            imports: [
                ReactiveFormsModule, FormsModule, RouterTestingModule
            ],
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(AddrecipesPage);
        component = fixture.componentInstance;
        // Component variables.
        component.user = mockFirebase.user;
        component.directions = [{'value': 'Add Rice to Broth.'}];
        component.ingredients = [{'name': 'Rice', 'measurements': {'value': 1, 'units': 'cups'}}] as Ingredients[];
        component.files = ['file1'];
        // Mocks
        popMock = spyOn(component, 'showPop');
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should not create card', () => {
        component.directions = [];
        component.ingredients = [];
        fixture.detectChanges();
        const cardElement = fixture.debugElement.nativeElement.querySelector('#ingredientsCard ion-card');
        const cardElement2 = fixture.debugElement.nativeElement.querySelector('#directionsCard ion-card');
        expect(cardElement).toBeFalsy();
        expect(cardElement2).toBeFalsy();
    });

    it('should create card', () => {
        const cardElement = fixture.debugElement.nativeElement.querySelector('#ingredientsCard ion-card');
        const cardElement2 = fixture.debugElement.nativeElement.querySelector('#directionsCard ion-card');
        expect(cardElement).toBeTruthy();
        expect(cardElement2).toBeTruthy();
    });

    it('should delete  ingredients or directions html element test', () => {
        const iconElement = fixture.debugElement.nativeElement.querySelector('#deleteIngredients');
        const iconElement2 = fixture.debugElement.nativeElement.querySelector('#deleteDirections');
        expect(iconElement).toBeTruthy();
        expect(iconElement2).toBeTruthy();
        iconElement.click();
        iconElement2.click();
        fixture.whenStable().then(() => {
            expect(component.ingredients.length).toEqual(0);
            expect(component.directions.length).toEqual(0);
        });
    });

    it('should duplicate ingredients or directions html element test', () => {
        const iconElement = fixture.debugElement.nativeElement.querySelector('#duplicateIngredients');
        const iconElement2 = fixture.debugElement.nativeElement.querySelector('#duplicateDirections');
        expect(iconElement).toBeTruthy();
        expect(iconElement2).toBeTruthy();
        iconElement.click();
        iconElement2.click();
        fixture.whenStable().then(() => {
            expect(component.ingredients.length).toEqual(2);
            expect(component.directions.length).toEqual(2);
        });
    });

    it('should edit ingredients or directions html element test', () => {
        const iconElement = fixture.debugElement.nativeElement.querySelector('#editIngredients');
        const iconElement2 = fixture.debugElement.nativeElement.querySelector('#editDirections');
        expect(iconElement).toBeTruthy();
        expect(iconElement2).toBeTruthy();
        iconElement.click();
        iconElement2.click();
        fixture.detectChanges();
        fixture.whenStable().then(() => {
            expect(popMock).toHaveBeenCalledTimes(2);
        });
    });

    it('should delete ingredients or directions method test', () => {
        component.delete(0, 'ingredients');
        expect(component.ingredients.length).toEqual(0);
        component.delete(0, 'directions');
        expect(component.directions.length).toEqual(0);
    });

    it('should duplicate ingredients or directions method test', () => {
        component.duplicate('ingredients', component.ingredients[0]);
        expect(component.ingredients.length).toEqual(2);
        component.duplicate('directions', component.directions[0]);
        expect(component.directions.length).toEqual(2);
    });

    it('should edit ingredients or directions method test', async () => {
        await component.edit(0, '', 'ingredients', component.ingredients[0]);
        fixture.detectChanges();
        fixture.whenStable().then(() => {
            expect(popMock).toHaveBeenCalled();
        });
        component.edit(0, '', 'directions', component.directions[0]);
        fixture.detectChanges();
        fixture.whenStable().then(() => {
            expect(popMock).toHaveBeenCalled();
        });
    });

    it('should disable form if view is not add/edit', () => {
        component.view_type = 'view';
        fixture.detectChanges();
        fixture.whenStable().then(() => {
            const fieldElement = fixture.debugElement.nativeElement.querySelector('#addFormFieldSet');
            const iconElement = fixture.debugElement.nativeElement.querySelector('#editIngredients');
            const iconElement2 = fixture.debugElement.nativeElement.querySelector('#editDirections');
            const imageElement = fixture.debugElement.nativeElement.querySelector('#imageSection');
            const buttonElement = fixture.debugElement.nativeElement.querySelector('#saveForm');
            expect(buttonElement).toBeFalsy();
            expect(fieldElement.disabled).toBeTruthy();
            expect(imageElement).toBeTruthy();
            expect(iconElement).toBeFalsy();
            expect(iconElement2).toBeFalsy();
        });
    });
});
