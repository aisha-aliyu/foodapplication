import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {ReactiveFormsModule, FormsModule} from '@angular/forms';
import {RouterTestingModule} from '@angular/router/testing';
import {CookieServiceMock} from '../../mocks/others';
import {CookieService} from 'ngx-cookie-service';

import {FirebaseMock} from '../../mocks/firebasemock';

import * as firebase from 'firebase/app';
import 'firebase/auth';

import {RegisterPage} from './register.page';
import {Location} from '@angular/common';
import { By } from '@angular/platform-browser';


describe('RegisterPage', () => {

    let mockFirebase: FirebaseMock = new FirebaseMock();
    let firebaseAuth;
    let firebaseConnect;
    let location: Location;

    let component: RegisterPage;
    let fixture: ComponentFixture<RegisterPage>;
    let registerBtn: HTMLButtonElement;


    beforeAll(() => {
        firebaseConnect = spyOn(firebase, 'database').and.returnValue(mockFirebase.database());
        firebaseAuth = spyOn(firebase, 'auth').and.returnValue(mockFirebase.auth());
    });

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [RegisterPage],
            imports: [ReactiveFormsModule, RouterTestingModule, FormsModule],
            providers: [{provide: CookieService, useClass: CookieServiceMock}],
            schemas: [CUSTOM_ELEMENTS_SCHEMA],
        })
            .compileComponents();
        fixture = TestBed.createComponent(RegisterPage);
        fixture.detectChanges();
        component = fixture.componentInstance;
        // component.registerForm.reset();

    }));

    it('should create', () => {
      expect(firebaseAuth).toBeTruthy();
      expect(component).toBeTruthy();
    });

    it('form invalid when empty', () => {
        expect(component.registerForm.valid).toBeFalsy();
    });

    it('email field validity', () => {
        let errors = {};
        let email = component.registerForm.controls['email'];
        errors = email.errors || {};
        expect(errors['required']).toBeTruthy();
    });

    it('email pattern validity', () => {
        let errors = {};
        let email = component.registerForm.controls['email'];
        email.setValue('test');
        errors = email.errors || {};
        expect(errors['pattern']).toBeFalsy();
        email.setValue('test@test.com');
        expect(errors['pattern']).toBeFalsy();
    });

    it('verify password match', () => {
        expect(component.registerForm.valid).toBeFalsy();
        component.registerForm.controls['email'].setValue(mockFirebase.user.email);
        component.registerForm.controls['pwd'].setValue(mockFirebase.password);
        component.registerForm.controls['cpwd'].setValue('testPassword');
        component.registerForm.controls['firstName'].setValue(mockFirebase.user.firstName);
        component.registerForm.controls['lastName'].setValue(mockFirebase.user.lastName);
        expect(component.registerForm.valid).toBeTruthy();

        component.register();

        expect(component.registerInfo).toBeTruthy();
        expect(component.message).toBe('Passwords do not match');
    });

    it('register user already exists', async(() => {
        expect(component.registerForm.valid).toBeFalsy();
        component.registerForm.controls['email'].setValue(mockFirebase.user.email);
        component.registerForm.controls['pwd'].setValue(mockFirebase.password);
        component.registerForm.controls['cpwd'].setValue(mockFirebase.password);
        component.registerForm.controls['firstName'].setValue(mockFirebase.user.firstName);
        component.registerForm.controls['lastName'].setValue(mockFirebase.user.lastName);

        expect(component.registerForm.valid).toBeTruthy();

        component.register();
        expect(firebaseAuth).toHaveBeenCalled();
        setTimeout(() => {
            expect(component.registerInfo).toBeTruthy();
            expect(component.message).toBeTruthy();
        });

    }));

    it('register user successfully', async(() => {


        location = TestBed.get(Location);
        location.go('register');
        expect(component.registerForm.valid).toBeFalsy();
        component.registerForm.controls['email'].setValue('test1@gmail.com');
        component.registerForm.controls['pwd'].setValue(mockFirebase.password);
        component.registerForm.controls['cpwd'].setValue(mockFirebase.password);
        component.registerForm.controls['firstName'].setValue(mockFirebase.user.firstName);
        component.registerForm.controls['lastName'].setValue(mockFirebase.user.lastName);
        expect(component.registerForm.valid).toBeTruthy();

        component.register();

        expect(firebaseAuth).toHaveBeenCalled()
        expect(firebaseConnect).toHaveBeenCalled()

        setTimeout(() => {
            expect(location.path()).toBe('/');
        });
    }));

    it('should disable login button if form is invalid', () => {
        registerBtn = fixture.debugElement.query(By.css('#registerButton')).nativeElement;
        component.registerForm.controls['email'].setValue('');
        component.registerForm.controls['pwd'].setValue('');
        component.registerForm.controls['cpwd'].setValue('');
        component.registerForm.controls['firstName'].setValue('');
        component.registerForm.controls['lastName'].setValue('');
        expect(component.registerForm.valid).toBeFalsy();
        expect(registerBtn.disabled).toBe(true);
    });



});
