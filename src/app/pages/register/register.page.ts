import {Component, OnInit} from '@angular/core';
import {Validators, FormBuilder} from '@angular/forms';
import {AuthenticationService} from '../../providers/authentication/authentication.service';
import {DatabaseService} from '../../providers/database/database.service';
import {Router} from '@angular/router';
import {Users} from '../../model/users/users';
import {del} from 'selenium-webdriver/http';


@Component({
    selector: 'app-register',
    templateUrl: './register.page.html',
    styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {

    public registerForm;
    public registerInfo = false;
    public message = '';
    public alert = '';

    constructor(private formBuilder: FormBuilder, private router: Router,
                private authenticationService: AuthenticationService, private databaseService: DatabaseService) {
        this.registerForm = formBuilder.group({
            firstName: ['', Validators.compose([Validators.required])],
            lastName: ['', Validators.compose([Validators.required])],
            email: ['', Validators.compose([Validators.required])],
            pwd: ['', Validators.compose([Validators.required])],
            cpwd: ['', Validators.compose([Validators.required])],
        });

    }

    ngOnInit() {
    }

    cancel() {
        this.router.navigateByUrl('/login');
    }

    register() {
        if (this.registerForm.valid) {
            if (this.registerForm.value.pwd !== this.registerForm.value.cpwd) {
                this.registerInfo = true;
                this.alert = 'alert-danger';
                this.message = 'Passwords do not match';
            } else {
                let authentication = this.authenticationService.addEmailAuth(this.registerForm.value.email, this.registerForm.value.pwd);
                authentication.then((res) => {
                    const user: Users = this.registerForm.value as Users;
                    user.role = 'users.ts';
                    user.uid = res.user.uid;
                    user.displayName = this.registerForm.value.firstName + ' ' + this.registerForm.value.lastName;
                    delete user['pwd'];
                    delete user['cpwd'];
                    this.authenticationService.user = user;
                    this.databaseService.saveUserDetails(user);
                    this.router.navigateByUrl('');

                }).catch((err) => {
                    this.registerInfo = true;
                    this.alert = 'alert-danger';
                    if (err.code) {
                        this.message = err.message;
                    } else {
                        this.message = 'An error occurred on the server while trying to register';
                    }
                });
            }
        } else {
            this.alert = 'alert-danger';
            this.registerInfo = true;
            this.message = 'Please fill all fields.';
        }
    }

    close() {
        this.registerInfo = false;
    }

}
