import {Component, OnChanges, OnInit, SimpleChange, SimpleChanges, ViewChild} from '@angular/core';
import {Router} from '@angular/router';
import {AuthenticationService} from '../../providers/authentication/authentication.service';
import {FormBuilder, Validators} from '@angular/forms';
import {NavController} from '@ionic/angular';
import {Users} from '../../model/users/users';
import {DatabaseService} from '../../providers/database/database.service';
import {ExtrasService} from '../../providers/extras/extras.service';


@Component({
    selector: 'app-viewalluser',
    templateUrl: './viewalluser.page.html',
    styleUrls: ['./viewalluser.page.scss']
})

export class ViewAllUserPage implements OnInit {
    public enableEdit = false;
    public userInfo = false;
    public firstUser = true;
    public allRole = ['Admin', 'User'];
    user_list = [];
    rowClicked;
    public userForm;
    user: Users;
    searchInput: String = '';
    old_firstName;
    old_lastName;
    old_email;
    old_role;
    suggested: number;
    favorites: number;
    public message = '';
    public alert;

    constructor(private router: Router,
                public authenticationService: AuthenticationService,
                private formBuilder: FormBuilder,
                public databaseProvider: DatabaseService,
                public extraService: ExtrasService) {
        this.userForm = this.formBuilder.group({
                firstName: ['', Validators.compose([Validators.required])],
                lastName: ['', Validators.compose([Validators.required])],
                email: ['', Validators.compose([Validators.required])],
                role: ['', Validators.compose([Validators.required])],
                uid: ['', Validators.compose([Validators.required])]
        });
        this.user = this.authenticationService.user;
        setTimeout(() => {
            this.extraService.initializeUser(this);
        });
    }

    ngOnInit() {
        this.getAllUsers();
        this.userForm.disable();
    }

    getAllUsers() {
        this.databaseProvider.getUserDetails().on('value', (snapshot) => {
            const users = snapshot.val();
            let userslist = [];
            for (let key in users) {
                if (users.hasOwnProperty(key)) {
                    userslist.push(users[key]);
                }
            }
            this.user_list = userslist;
            console.log(this.user_list)
            if (this.user_list.length > 0 && this.firstUser) {
                this.showSpecificUser(this.user_list[0]);
                this.firstUser = false;
            }
        });
    }

    showSpecificUser(user) {
        if (user) {
            console.log(user);
            user.user_click = true;
            this.userForm.controls['firstName'].setValue(user.firstName);
            this.userForm.controls['lastName'].setValue(user.lastName);
            this.userForm.controls['email'].setValue(user.email);
            this.userForm.controls['role'].setValue(user.role);
            this.userForm.controls['uid'].setValue(user.uid);
            this.suggested = user.suggested;
            this.favorites = user.favorites;
        }
    }

    changeTableRowColor(idx: any) {
        if (this.rowClicked === idx) {
            this.rowClicked = -1;
        } else {
            this.rowClicked = idx;
        }
    }

    delete(uid) {
        this.databaseProvider.deleteUser(uid);
    }

    edit() {
        this.enableEdit = true;
        this.userForm.enable();
        this.old_firstName = this.userForm.value.firstName;
        this.old_lastName = this.userForm.value.lastName;
        this.old_email = this.userForm.value.email;
        this.old_role = this.userForm.value.role;
    }

    cancel() {
        this.enableEdit = false;
        this.userInfo = false;
        this.userForm.disable();
        this.userForm.controls['firstName'].setValue(this.old_firstName);
        this.userForm.controls['lastName'].setValue(this.old_lastName);
        this.userForm.controls['email'].setValue(this.old_email);
        this.userForm.controls['role'].setValue(this.old_role);
    }

    save() {
        if (this.userForm.valid) {
            this.enableEdit = false;
            this.userForm.disable();
            const user: Users = this.userForm.value as Users;
            this.userInfo = true;
            this.databaseProvider.updateUserDetails(user);
        } else {
            this.alert = 'alert-danger';
            this.userInfo = true;
            this.message = 'Please fill all fields.';
        }
    }

    close() {
        this.userInfo = false;
    }

    filterUsers(searchValue) {
        if (searchValue) {
        }
    }

    filterUserSearch(event) {
        this.filterUsers(event);
    }
}
