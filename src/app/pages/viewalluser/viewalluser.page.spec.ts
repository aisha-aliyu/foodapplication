import {CUSTOM_ELEMENTS_SCHEMA, DebugElement, Injector} from '@angular/core';
import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ViewAllUserPage} from './viewalluser.page';
import {ReactiveFormsModule, FormsModule} from '@angular/forms';
import {RouterTestingModule} from '@angular/router/testing';
import {FirebaseMock} from '../../mocks/firebasemock';
import * as firebase from 'firebase/app';
import 'firebase/database';
import {By} from '@angular/platform-browser';
import {Users} from '../../model/users/users';
import {CookieServiceMock, PopupMock} from '../../mocks/others';
import {CookieService} from 'ngx-cookie-service';
import {DatabaseService} from '../../providers/database/database.service';
import {AuthenticationService} from '../../providers/authentication/authentication.service';
import {ExtrasService} from '../../providers/extras/extras.service';
import {PopoverController} from '@ionic/angular';

describe('ViewAllUserPage', () => {
    let dbService: DatabaseService;
    let authService: AuthenticationService;
    let extraService: ExtrasService;
    let firebaseInitialize;
    let component: ViewAllUserPage;
    let fixture: ComponentFixture<ViewAllUserPage>;
    let debugElement: DebugElement;
    const mockFirebase: FirebaseMock = new FirebaseMock();
    let firebaseConnect;
    let firebaseAuth;

    beforeAll(() => {
        mockFirebase.userInfo = mockFirebase.user;
        firebaseConnect = spyOn(firebase, 'database').and.returnValue(mockFirebase.database());
        firebaseAuth = spyOn(firebase, 'auth').and.returnValue(mockFirebase.auth());
        firebaseInitialize = spyOn(firebase, 'initializeApp').and.returnValue(mockFirebase.initializeApp({}));
    });

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ViewAllUserPage],
            schemas: [CUSTOM_ELEMENTS_SCHEMA],
            providers: [{provide: CookieService, useClass: CookieServiceMock}, {provide: PopoverController, useClass: PopupMock}],
            imports: [
                ReactiveFormsModule, FormsModule, RouterTestingModule
            ]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ViewAllUserPage);
        component = fixture.componentInstance;
        fixture.detectChanges();
        dbService = TestBed.get(DatabaseService);
        authService = TestBed.get(AuthenticationService);
        extraService = TestBed.get(ExtrasService);
    });

    it('should create', () => {
        expect(firebaseConnect).toBeTruthy();
        expect(component).toBeTruthy();
        console.log('component');
        console.log(component);
    });

    it('user form invalid when empty', () => {
        expect(component.userForm.valid).toBeFalsy();
    });

    it('user form should have four fields', () => {
        expect(component.userForm.value.firstName).toBeTruthy();
        expect(component.userForm.value.lastName).toBeTruthy();
        expect(component.userForm.value.email).toBeTruthy();
        expect(component.userForm.value.role).toBeTruthy();
        expect(component.userForm.value.test).toBeFalsy();
    });

    it('user form fields should be disabled if edit is not clicked', () => {
        let email = component.userForm.controls['email'];
        let fisrtname = component.userForm.controls['firstName'];
        let lastname = component.userForm.controls['lastName'];
        let role = component.userForm.controls['role'];
        expect(email.status).toBe('DISABLED');
        expect(fisrtname.status).toBe('DISABLED');
        expect(lastname.status).toBe('DISABLED');
        expect(role.status).toBe('DISABLED');
    });

    it('user form fields should not be disabled if edit is clicked', () => {
        let email = component.userForm.controls['email'];
        let fisrtname = component.userForm.controls['firstName'];
        let lastname = component.userForm.controls['lastName'];
        let role = component.userForm.controls['role'];
        component.edit();
        expect(email.status).toBe('VALID');
        expect(fisrtname.status).toBe('VALID');
        expect(lastname.status).toBe('VALID');
        expect(role.status).toBe('VALID');
    });

    it('user form cannot be saved if one or more fields are invalid' , () => {
        expect(component.userInfo).toBeFalsy();
        expect(component.enableEdit).toBeFalsy();
        component.edit();
        expect(component.enableEdit).toBeTruthy();
        component.userForm.controls['email'].setValue('');
        component.userForm.controls['firstName'].setValue('');
        component.userForm.controls['lastName'].setValue('');
        component.userForm.controls['role'].setValue('');
        component.userForm.controls['uid'].setValue('');
        component.save();
        expect(component.userInfo).toBeTruthy();
        expect(component.message).toBe('Please fill all fields.');
        expect(component.enableEdit).toBeTruthy();
    });

    it('user form can be saved' , () => {
        expect(component.userInfo).toBeFalsy();
        expect(component.enableEdit).toBeFalsy();
        component.edit();
        expect(component.enableEdit).toBeTruthy();
        component.userForm.controls['email'].setValue('mock@gmail.com');
        component.userForm.controls['firstName'].setValue('mock1');
        component.userForm.controls['lastName'].setValue('mock2');
        component.userForm.controls['role'].setValue('admin');
        component.save();
        expect(firebaseConnect).toHaveBeenCalled();
        expect(component.enableEdit).toBeFalsy();
        expect(component.userForm.value.firstName).toBe('mock1');
        expect(component.userForm.value.lastName).toBe('mock2');
        expect(component.userForm.value.email).toBe('mock@gmail.com');
        expect(component.userForm.value.role).toBe('admin');
    });

    it('user form fields should change back into old values after click cancel' , () => {
        expect(component.enableEdit).toBeFalsy();
        component.edit();
        expect(component.enableEdit).toBeTruthy();
        component.userForm.controls['email'].setValue('test@ibm.com');
        component.userForm.controls['firstName'].setValue('test');
        component.userForm.controls['lastName'].setValue('test');
        component.userForm.controls['role'].setValue('admin');
        component.cancel();
        expect(component.enableEdit).toBeFalsy();
        expect(component.userForm.value.firstName).toBe('mock1');
        expect(component.userForm.value.lastName).toBe('mock2');
        expect(component.userForm.value.email).toBe('mock@gmail.com');
        expect(component.userForm.value.role).toBe('admin');
    });

});