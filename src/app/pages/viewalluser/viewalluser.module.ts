import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {Routes, RouterModule} from '@angular/router';

import {IonicModule} from '@ionic/angular';

import {ViewAllUserPage} from './viewalluser.page';
import {NavbarPageModule} from '../../partials/navbar/navbar.module';

const routes: Routes = [
    {
        path: '',
        component: ViewAllUserPage
    }
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        IonicModule,
        RouterModule.forChild(routes),
        NavbarPageModule
    ],
    declarations: [ViewAllUserPage],
    exports: []
})
export class ViewAllUserPageModule {
}