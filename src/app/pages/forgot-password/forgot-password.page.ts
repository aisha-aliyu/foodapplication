import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import {AuthenticationService} from '../../providers/authentication/authentication.service';
import {DatabaseService} from '../../providers/database/database.service';
import {Router} from '@angular/router';


@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.page.html',
  styleUrls: ['./forgot-password.page.scss'],
})
export class ForgotPasswordPage implements OnInit {

    public resetForm;
    alert = '';
    message = '';
    resp = false;

  constructor(private formBuilder: FormBuilder, private router: Router,
              private authenticationService: AuthenticationService) {
      this.resetForm = formBuilder.group({
          email: ['', Validators.compose([Validators.required])],
      });
  }

  ngOnInit() {
  }

  cancel() {
      this.router.navigateByUrl('/login');
  }

  resetPassword() {
      if (this.resetForm.value.email) {
          this.authenticationService.sendPasswordResetEmail(this.resetForm.value.email).then((result) => {
              this.resp = true;
              this.alert = 'alert-success';
              this.message = 'Reset password email has been sent. Please check your email!';
          }).catch( (error) => {
              this.resp = true;
              this.alert = 'alert-danger';
              if (error.code) {
                  this.message = error.message;
              } else {
                  this.message = 'Errors occurred when sending password reset email!';
              }
          });
      } else {
          this.resp = true;
          this.message = 'Please provide your registered email.';

      }
  }

  close() {
    this.resp = false;
  }

}
