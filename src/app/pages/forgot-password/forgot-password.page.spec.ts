import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {FirebaseMock} from '../../mocks/firebasemock';
import {ForgotPasswordPage} from './forgot-password.page';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {RouterTestingModule} from '@angular/router/testing';
import * as firebase from 'firebase/app';
import {CookieService} from 'ngx-cookie-service';
import {CookieServiceMock} from '../../mocks/others';

describe('ForgotPasswordPage', () => {
    let component: ForgotPasswordPage;
    let fixture: ComponentFixture<ForgotPasswordPage>;
    let mockFirebase: FirebaseMock = new FirebaseMock();
    let firebaseAuth;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ForgotPasswordPage],
            imports: [ReactiveFormsModule, RouterTestingModule, FormsModule],
            schemas: [CUSTOM_ELEMENTS_SCHEMA],
            providers: [{provide: CookieService, useClass: CookieServiceMock}]
        });
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ForgotPasswordPage);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    beforeAll(() => {
        firebaseAuth = spyOn(firebase, 'auth').and.returnValue(mockFirebase.auth());
    });

    it('should create', () => {
        expect(component).toBeDefined();
    });

    it('should not send reset password email if email is empty', () => {
        component.resetForm.controls['email'].setValue(null);
        expect(component.resp).toEqual(false);
        expect(component).toBeDefined();
        component.resetPassword();
        setTimeout(() => {
            expect(component.resp).toEqual(true);
            expect(component.alert).toBe('');
            expect(component.message).toEqual('Please provide your registered email.');
        });
    });

    it('should not send reset password email if email is badly format', () => {
        component.resetForm.controls['email'].setValue('fake1');
        expect(component.resp).toEqual(false);
        expect(component.alert).toBe('');
        expect(component).toBeDefined();
        component.resetPassword();
        setTimeout(() => {
            expect(component.resp).toEqual(true);
            expect(component.alert).toEqual('alert-danger');
            expect(component.message).toEqual('The email address is badly formatted.');
        });
    });

    it('should not send reset password email if not matched', () => {
        component.resetForm.controls['email'].setValue('fake@gmail.com');
        expect(component.resp).toEqual(false);
        expect(component.alert).toBe('');
        expect(component).toBeDefined();
        component.resetPassword();
        setTimeout(() => {
            expect(component.resp).toEqual(true);
            expect(component.alert).toEqual('alert-danger');
            expect(component.message).toEqual('There is no user record corresponding to this identifier. The user may have been deleted.');
        });
    });

    it('should send reset password email', () => {
        component.resetForm.controls['email'].setValue('test@gmail.com');
        expect(component.resp).toEqual(false);
        expect(component.alert).toBe('');
        expect(component).toBeDefined();
        component.resetPassword();
        setTimeout(() => {
            expect(component.resp).toEqual(true);
            expect(component.alert).toEqual('alert-success');
            expect(component.message).toEqual('Reset password email has been sent. Please check your email!');
        });
    });


});