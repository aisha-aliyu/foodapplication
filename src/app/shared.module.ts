import {NgModule} from '@angular/core';
import {NavbarPage} from './partials/navbar/navbar.page';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {IonicModule} from '@ionic/angular';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule
    ],
    declarations: [NavbarPage],
    exports: [NavbarPage]
})
export class SharedModule {
}
