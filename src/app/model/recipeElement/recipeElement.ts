
export interface RecipeElement {
    name: string;
    category: string;
    numOfViews: number;
}
