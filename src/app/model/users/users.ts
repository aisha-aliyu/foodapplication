export class Users {

    private _uid: string;
    private _email: string;
    private _firstName: string;
    private _lastName: string;
    private _displayName: string;
    private _photoURL?: string;
    private _role: string;
    private _suggested: number;
    private _favorites: number;
    private _providerData?: any;

    private _user: Users;


    constructor() {

    }

    get uid(): string {
        return this._uid;
    }

    set uid(value: string) {
        this._uid = value;
    }

    get email(): string {
        return this._email;
    }

    set email(value: string) {
        this._email = value;
    }

    get displayName(): string {
        return this._displayName;
    }

    set displayName(value: string) {
        this._displayName = value;
    }


    get firstName(): string {
        return this._firstName;
    }

    set firstName(value: string) {
        this._firstName = value;
    }


    get lastName(): string {
        return this._lastName;
    }

    set lastName(value: string) {
        this._lastName = value;
    }

    get photoURL(): string {
        return this._photoURL;
    }

    set photoURL(value: string) {
        this._photoURL = value;
    }

    get role(): string {
        return this._role;
    }

    set role(value: string) {
        this._role = value;
    }

    get suggested(): number {
        return this._suggested;
    }

    set suggested(value: number) {
        this._suggested = value;
    }

    get favorites(): number {
        return this._favorites;
    }

    set favorites(value: number) {
        this._favorites = value;
    }

    get providerData(): any {
        return this._providerData;
    }

    set providerData(value: any) {
        this._providerData = value;
    }

    get user(): Users {
        return this._user;
    }

    set user(value: Users) {
        this._user = value;
    }
}
