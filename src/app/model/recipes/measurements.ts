export interface Measurements {
    value: number;
    units: string;
}