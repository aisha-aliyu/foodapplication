import {Measurements} from './measurements';

export interface Ingredients {
    name: string;
    measurements: Measurements;
}
