import {Measurements} from './measurements';
import {Ingredients} from './ingredients';
import {Directions} from './directions';

export class Recipes {

    id: string;
    name: string;
    description: string;
    country: string;
    category: string;
    createdBy: string;
    noOfViews: number = 0;
    imageURLs: [string];
    difficulty: string;
    spicy: number;
    preparation: Measurements;
    cooking: Measurements;
    ingredients: [Ingredients];
    directions: [Directions];
    suggested: boolean;

    constructor(id: string, name: string, description: string, country: string, category: string, createdBy: string, imageURLs: [string], difficulty: string, spicy: number, preparation: Measurements, cooking: Measurements, ingredients: [Ingredients], directions: [Directions], suggested: boolean) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.country = country;
        this.category = category;
        this.createdBy = createdBy;
        this.imageURLs = imageURLs;
        this.difficulty = difficulty;
        this.spicy = spicy;
        this.preparation = preparation;
        this.cooking = cooking;
        this.ingredients = ingredients;
        this.directions = directions;
        this.suggested = suggested;
    }

}
