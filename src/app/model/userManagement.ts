

export class User {
    email: string;
    firstname: string;
    lastname: string;
    role: string;

    constructor(json) {
        this.email = json.email;
        this.firstname = json.firstname;
        this.lastname = json.lastname;
        this.role = json.role;
    }
}
