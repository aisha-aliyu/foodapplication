import {Component} from '@angular/core';

import {NavController, Platform} from '@ionic/angular';
import {SplashScreen} from '@ionic-native/splash-screen/ngx';
import {StatusBar} from '@ionic-native/status-bar/ngx';
import {Router} from '@angular/router';

/*Providers import*/
import * as firebase from 'firebase/app';
import 'firebase/auth';

import * as firebase_configuration from './configurations/credentials.json';
import {AuthenticationService} from './providers/authentication/authentication.service';
import {Users} from './model/users/users';
import {DatabaseService} from './providers/database/database.service';

@Component({
    selector: 'app-root',
    templateUrl: 'app.component.html',
    styleUrls: ['app.component.scss'],
})
export class AppComponent {
    constructor(
        private platform: Platform,
        private splashScreen: SplashScreen,
        private statusBar: StatusBar, private router: Router, private authService: AuthenticationService) {
        this.initializeApp();
    }

    initializeApp() {
        this.platform.ready().then(() => {
            this.statusBar.styleDefault();
            this.splashScreen.hide();
            firebase.initializeApp(firebase_configuration['firebase']);
            this.userVerification();
        });
    }

    userVerification() {
        this.authService.getUser().then((authUser) => {
            const databaseProvider = new DatabaseService();
            if (authUser) {
                const uid = authUser['uid'];
                databaseProvider.getSpecificUserDetails(uid).on('value', (snapshot) => {
                    const user = snapshot.val()[uid];
                    this.authService.user = user as Users;
                });
            } else {
                this.router.navigateByUrl('login');
            }
        }).catch(() => {
            this.router.navigateByUrl('login');
        });
    }
}

