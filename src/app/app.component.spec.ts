import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {TestBed, async, ComponentFixture} from '@angular/core/testing';
import {Location} from '@angular/common';

import {RouterModule} from '@angular/router';

import {NavController, Platform} from '@ionic/angular';
import {SplashScreen} from '@ionic-native/splash-screen/ngx';
import {StatusBar} from '@ionic-native/status-bar/ngx';

import {AppComponent} from './app.component';

import {RouterTestingModule} from '@angular/router/testing';
import {FirebaseMock} from './mocks/firebasemock';

import * as firebase from 'firebase/app';
import 'firebase/auth';
import {CookieServiceMock, LocationMock} from './mocks/others';
import {Router} from '@angular/router';
import {CookieService} from 'ngx-cookie-service';
import {PopupsPage} from './partials/popups/popups.page';
import {By} from '@angular/platform-browser';

describe('AppComponent', () => {

    let statusBarSpy, splashScreenSpy, platformReadySpy, platformSpy;

    const mockFirebase: FirebaseMock = new FirebaseMock();
    let firebaseInitialize;
    let firebaseAuth;

    let component: AppComponent;
    let fixture: ComponentFixture<AppComponent>;
    let location: Location;

    const mockNavControl: LocationMock = new LocationMock();

    beforeAll(() => {
        firebaseInitialize = spyOn(firebase, 'initializeApp').and.returnValue(mockFirebase.initializeApp({}));
        firebaseAuth = spyOn(firebase, 'auth').and.returnValue(mockFirebase.auth());
    });

    beforeEach(async(() => {
        statusBarSpy = jasmine.createSpyObj('StatusBar', ['styleDefault']);
        splashScreenSpy = jasmine.createSpyObj('SplashScreen', ['hide']);
        platformReadySpy = Promise.resolve();
        platformSpy = jasmine.createSpyObj('Platform', {ready: platformReadySpy});

        TestBed.configureTestingModule({
            declarations: [AppComponent],
            schemas: [CUSTOM_ELEMENTS_SCHEMA],
            imports: [RouterTestingModule, RouterModule],
            providers: [
                {provide: StatusBar, useValue: statusBarSpy},
                {provide: SplashScreen, useValue: splashScreenSpy},
                {provide: Platform, useValue: platformSpy},
                {provide: NavController, useValue: mockNavControl},
                {provide: Router, useValue: mockNavControl},
                {provide: CookieService, useClass: CookieServiceMock},
            ],
        }).compileComponents();
    }));

    beforeEach(() => {
        // get the location module
        location = TestBed.get(Location);
        mockFirebase.userInfo = null;

        mockNavControl.location = location;
        // create component and test fixture
        fixture = TestBed.createComponent(AppComponent);

        // get test component from the fixture
        component = fixture.componentInstance;

        location.go('');
    });

    it('should create the app', () => {
        const app = fixture.debugElement.componentInstance;
        expect(app).toBeTruthy();
    });

    it('should initialize the app', async () => {
        expect(platformSpy.ready).toHaveBeenCalled();
        expect(firebaseInitialize).toHaveBeenCalled();
        await platformReadySpy;
        expect(statusBarSpy.styleDefault).toHaveBeenCalled();
        expect(splashScreenSpy.hide).toHaveBeenCalled();
        expect(firebaseAuth).toHaveBeenCalled();
    });

    it('should navigate to login if user does not exist', async () => {
        component.userVerification();
        setTimeout(() => {
            expect(firebaseAuth).toHaveBeenCalled();
            expect(location.path()).toBe('/login');
        });
    });

    it('should remain in home if user exist', async () => {
        mockFirebase.userInfo = mockFirebase.user;
        component.userVerification();
        fixture.detectChanges();
        fixture.whenStable().then(() => {
            setTimeout(() => {
                expect(firebaseAuth).toHaveBeenCalled();
                expect(location.path()).toBe('');
            });
        });
    });
});
