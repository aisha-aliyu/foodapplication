import {CUSTOM_ELEMENTS_SCHEMA, DebugElement} from '@angular/core';
import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {PopupsPage} from './popups.page';
import {Router} from '@angular/router';
import {CookieServiceMock, LocationMock, NavParamsMock, PopupMock} from '../../mocks/others';
import {NavParams, PopoverController} from '@ionic/angular';
import {CookieService} from 'ngx-cookie-service';

import * as firebase from 'firebase/app';
import {FirebaseMock} from '../../mocks/firebasemock';
import {By} from '@angular/platform-browser';
import {Location} from '@angular/common';
import {RouterTestingModule} from '@angular/router/testing';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

describe('PopupsPage', () => {
    let component: PopupsPage;
    let fixture: ComponentFixture<PopupsPage>;

    let firebaseConnect;
    let firebaseAuth;
    const mockNavControl: LocationMock = new LocationMock();
    const mockFirebase: FirebaseMock = new FirebaseMock();
    let debugElement: DebugElement;
    let location: Location;
    let dismissMock;

    beforeAll(() => {
        firebaseConnect = spyOn(firebase, 'database').and.returnValue(mockFirebase.database());
        firebaseAuth = spyOn(firebase, 'auth').and.returnValue(mockFirebase.auth());
    });

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [PopupsPage],
            schemas: [CUSTOM_ELEMENTS_SCHEMA],
            imports: [RouterTestingModule, ReactiveFormsModule, FormsModule],
            providers: [{provide: NavParams, useClass: NavParamsMock}, {
                provide: CookieService,
                useClass: CookieServiceMock
            }, {provide: Router, useValue: mockNavControl}, {provide: PopoverController, useClass: PopupMock}]
        })
            .compileComponents();

        location = TestBed.get(Location);
        mockNavControl.location = location;
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(PopupsPage);
        component = fixture.componentInstance;
        fixture.detectChanges();
        // get the location module

        debugElement = fixture.debugElement.query(By.css('ion-list'));
        dismissMock = spyOn(component, 'dismiss');
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should not show list if type is not signout and user info does not exist', () => {
        expect(debugElement).toBeFalsy();
    });

    it('should show list if type is signout and user info exist', () => {
        component.type = 'signout';
        component.user = mockFirebase.user;
        fixture.detectChanges();
        const newDebugElement = fixture.debugElement.query(By.css('ion-list'));
        const headerDebugElement = fixture.debugElement.query(By.css('ion-list ion-list-header'));
        expect(newDebugElement).toBeTruthy();
        expect(headerDebugElement).toBeTruthy();
        const user_detail = `${mockFirebase.user.displayName} (${mockFirebase.user.email})`;
        const header_content = headerDebugElement.nativeElement.textContent.trim();
        expect(header_content).toEqual(user_detail);
    });

    it('should signout user', () => {
        component.signOut();
        fixture.detectChanges();
        expect(dismissMock).toHaveBeenCalledWith(null);
        const newDebugElement = fixture.debugElement.query(By.css('ion-list'));
        expect(newDebugElement).toBeFalsy();
        expect(location.path()).toBe('/login');
    });

    // Ingredients testing
    it('should not setup ingredients section', () => {
        const newDebugElement = fixture.debugElement.query(By.css('#ingredientSection'));
        const formElement = fixture.debugElement.query(By.css('#ingredientSection form'));
        const inputElement = fixture.debugElement.query(By.css('#ingredientSection ion-input'));
        const titleElement = fixture.debugElement.query(By.css('#ingredientSection h3'));
        const buttonElement = fixture.debugElement.query(By.css('#cancelIngredients'));
        const buttonElement2 = fixture.debugElement.query(By.css('#saveIngredients'));

        expect(newDebugElement).toBeFalsy();
        expect(formElement).toBeFalsy();
        expect(inputElement).toBeFalsy();
        expect(titleElement).toBeFalsy();
        expect(buttonElement).toBeFalsy();
        expect(buttonElement2).toBeFalsy();
    });

    it('should setup ingredients section', () => {
        component.type = 'ingredients';
        fixture.detectChanges();
        const newDebugElement = fixture.debugElement.query(By.css('#ingredientSection'));
        const titleElement = fixture.debugElement.query(By.css('#ingredientSection h3'));
        const formElement = fixture.debugElement.query(By.css('#ingredientSection form'));
        const inputElement = fixture.debugElement.query(By.css('#ingredientSection ion-input'));
        const buttonElement = fixture.debugElement.nativeElement.querySelector('#cancelIngredients');
        const buttonElement2 = fixture.debugElement.nativeElement.querySelector('#saveIngredients');
        expect(newDebugElement).toBeTruthy();
        expect(titleElement).toBeTruthy();
        expect(formElement).toBeTruthy();
        expect(inputElement).toBeTruthy();
        expect(buttonElement).toBeTruthy();
        expect(buttonElement2).toBeTruthy();
        expect(buttonElement2.disabled).toBeTruthy();
        const header_content = titleElement.nativeElement.textContent.trim();
        expect(header_content).toEqual('Ingredients');
    });

    it('should cancel ingredients section', () => {
        component.type = 'ingredients';
        fixture.detectChanges();
        const buttonElement = fixture.debugElement.nativeElement.querySelector('#cancelIngredients');
        buttonElement.click();
        fixture.whenStable().then(() => {
            expect(dismissMock).toHaveBeenCalledWith(null);
        });
    });

    it('should not save ingredients if form is invalid', () => {
        component.type = 'ingredients';
        fixture.detectChanges();
        component.ingredientsForm.controls['name'].setValue('');
        component.ingredientsForm.controls['value'].setValue('');
        component.ingredientsForm.controls['units'].setValue('');
        fixture.detectChanges();
        const buttonElement = fixture.debugElement.nativeElement.querySelector('#saveIngredients');
        expect(buttonElement.disabled).toBeTruthy();
    });

    it('should not save ingredients if value is not a number', () => {
        component.type = 'ingredients';
        fixture.detectChanges();
        component.ingredientsForm.controls['name'].setValue('');
        component.ingredientsForm.controls['value'].setValue('One');
        component.ingredientsForm.controls['units'].setValue('');
        fixture.detectChanges();
        const buttonElement = fixture.debugElement.nativeElement.querySelector('#saveIngredients');
        expect(buttonElement.disabled).toBeTruthy();
    });

    it('should save ingredients if form is valid', () => {
        component.type = 'ingredients';
        fixture.detectChanges();
        component.ingredientsForm.controls['name'].setValue('Rice');
        component.ingredientsForm.controls['value'].setValue(1);
        component.ingredientsForm.controls['units'].setValue('cup');
        fixture.detectChanges();
        const buttonElement = fixture.debugElement.nativeElement.querySelector('#saveIngredients');
        expect(buttonElement.disabled).toBeFalsy();
        buttonElement.click();
        fixture.whenStable().then(() => {
            expect(dismissMock).toHaveBeenCalledWith(component.ingredientsForm);
        });
    });

    //Directions testing
    it('should not setup directions section', () => {
        const newDebugElement = fixture.debugElement.query(By.css('#directionSection'));
        const formElement = fixture.debugElement.query(By.css('#directionSection form'));
        const inputElement = fixture.debugElement.query(By.css('#directionSection ion-input'));
        const titleElement = fixture.debugElement.query(By.css('#directionSection h3'));
        const buttonElement = fixture.debugElement.query(By.css('#cancelDirections'));
        const buttonElement2 = fixture.debugElement.query(By.css('#saveDirections'));

        expect(newDebugElement).toBeFalsy();
        expect(formElement).toBeFalsy();
        expect(inputElement).toBeFalsy();
        expect(titleElement).toBeFalsy();
        expect(buttonElement).toBeFalsy();
        expect(buttonElement2).toBeFalsy();
    });

    it('should setup directions section', () => {
        component.type = 'directions';
        fixture.detectChanges();
        const newDebugElement = fixture.debugElement.query(By.css('#directionSection'));
        const titleElement = fixture.debugElement.query(By.css('#directionSection h3'));
        const formElement = fixture.debugElement.query(By.css('#directionSection form'));
        const inputElement = fixture.debugElement.query(By.css('#directionSection ion-input'));
        const buttonElement = fixture.debugElement.nativeElement.querySelector('#cancelDirections');
        const buttonElement2 = fixture.debugElement.nativeElement.querySelector('#saveDirections');
        expect(newDebugElement).toBeTruthy();
        expect(titleElement).toBeTruthy();
        expect(formElement).toBeTruthy();
        expect(inputElement).toBeTruthy();
        expect(buttonElement).toBeTruthy();
        expect(buttonElement2).toBeTruthy();
        expect(buttonElement2.disabled).toBeTruthy();
        const header_content = titleElement.nativeElement.textContent.trim();
        expect(header_content).toEqual('Directions');
    });

    it('should cancel directions section', () => {
        component.type = 'directions';
        fixture.detectChanges();
        const buttonElement = fixture.debugElement.nativeElement.querySelector('#cancelDirections');
        buttonElement.click();
        fixture.whenStable().then(() => {
            expect(dismissMock).toHaveBeenCalledWith(null);
        });
    });

    it('should not save directions if form is invalid', () => {
        component.type = 'directions';
        fixture.detectChanges();
        component.directionsForm.controls['value'].setValue('');
        fixture.detectChanges();
        const buttonElement = fixture.debugElement.nativeElement.querySelector('#saveDirections');
        expect(buttonElement.disabled).toBeTruthy();
    });
    it('should save directions if form is valid', () => {
        component.type = 'directions';
        fixture.detectChanges();
        component.directionsForm.controls['value'].setValue('Add Rice to broth');
        fixture.detectChanges();
        const buttonElement = fixture.debugElement.nativeElement.querySelector('#saveDirections');
        expect(buttonElement.disabled).toBeFalsy();
        buttonElement.click();
        fixture.whenStable().then(() => {
            expect(dismissMock).toHaveBeenCalledWith(component.directionsForm);
        });
    });
});
