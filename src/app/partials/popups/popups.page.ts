import {Component, OnInit} from '@angular/core';
import {NavParams, PopoverController} from '@ionic/angular';
import {AuthenticationService} from '../../providers/authentication/authentication.service';
import {Users} from '../../model/users/users';
import {Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Ingredients} from '../../model/recipes/ingredients';
import {Measurements} from '../../model/recipes/measurements';
import {Directions} from '../../model/recipes/directions';

@Component({
    selector: 'app-popups',
    templateUrl: './popups.page.html',
    styleUrls: ['./popups.page.scss'],
})
export class PopupsPage implements OnInit {
    type: string;
    user: Users;
    public ingredientsForm: FormGroup;
    public directionsForm: FormGroup;

    constructor(public popCtrl: PopoverController, public authenticationService: AuthenticationService, public navParams: NavParams,
                private router: Router, private formBuilder: FormBuilder) {
        this.type = navParams.get('type');
        this.user = navParams.get('user');
        const extras = navParams.get('extras');
        this.ingredientsForm = formBuilder.group({
            name: [extras && extras['name'] ? extras['name'] as string : null,
                Validators.compose([Validators.required])],
            value: [extras && extras['measurements'] && extras['measurements']['value'] ? extras['measurements']['value'] as number : null,
                Validators.compose([Validators.required])],
            units: [extras && extras['measurements'] && extras['measurements']['units'] ? extras['measurements']['units']  as string : null,
                Validators.compose([Validators.required])],
        });
        this.directionsForm = formBuilder.group({
            value: [extras && extras['value'] ? extras['value'] as string : null,
                Validators.compose([Validators.required])]
        });
    }

    ngOnInit() {
    }

    async dismiss(form) {
        try {
            if (form) {
                if (form.valid) {
                    if (this.type === 'ingredients') {
                        const ingredientData: Ingredients = {
                            name: form.value.name,
                            measurements: {value: form.value.value, units: form.value.units} as Measurements
                        } as Ingredients;
                        await this.popCtrl.dismiss(ingredientData);
                    } else {
                        const directionData: Directions = {
                            value: form.value.value
                        } as Directions;
                        await this.popCtrl.dismiss(directionData);
                    }

                } else {
                    await this.popCtrl.dismiss(null);
                }
            } else {
                await this.popCtrl.dismiss(form);
            }
        } catch (e) {

        }
    }

    signOut() {
        this.dismiss(null);
        this.authenticationService.signOut(this.router);

    }
}
