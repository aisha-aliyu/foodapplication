import {CUSTOM_ELEMENTS_SCHEMA, DebugElement, Injector} from '@angular/core';
import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {NavbarPage} from './navbar.page';
import {FirebaseMock} from '../../mocks/firebasemock';
import {By} from '@angular/platform-browser';
import {PopoverController} from '@ionic/angular';
import {CookieServiceMock, LocationMock, PopupMock} from '../../mocks/others';
import {CookieService} from 'ngx-cookie-service';
import * as firebase from 'firebase/app';
import {Location} from '@angular/common';

describe('NavbarPage Without Information', () => {
    let component: NavbarPage;
    let fixture: ComponentFixture<NavbarPage>;
    let debugElement: DebugElement;

    const mockFirebase: FirebaseMock = new FirebaseMock();
    const mockNavControl: LocationMock = new LocationMock();
    let firebaseInitialize;
    let firebaseAuth;
    let firebaseConnect;

    beforeAll(() => {
        firebaseInitialize = spyOn(firebase, 'initializeApp').and.returnValue(mockFirebase.initializeApp({}));
        firebaseAuth = spyOn(firebase, 'auth').and.returnValue(mockFirebase.auth());
        firebaseConnect = spyOn(firebase, 'database').and.returnValue(mockFirebase.database());
    });


    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [NavbarPage],
            schemas: [CUSTOM_ELEMENTS_SCHEMA],
            providers: [{provide: CookieService, useClass: CookieServiceMock}, {
                provide: PopoverController,
                useClass: PopupMock
            }, {provide: Location, useValue: mockNavControl}]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(NavbarPage);
        component = fixture.componentInstance;
        fixture.detectChanges();

        debugElement = fixture.debugElement.query(By.css('ion-nav'));
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should not be displayed if user information is not available', () => {
        expect(component.user).toBeUndefined();
        expect(component.title).toEqual('Food Recipe');
        expect(component.search).toBeFalsy();
        expect(debugElement).toBeNull();
    });
});


describe('NavbarPage With User Information', () => {
    let component: NavbarPage;
    let fixture: ComponentFixture<NavbarPage>;
    let debugElement: DebugElement;
    let searchElement: DebugElement;
    let titleElement: DebugElement;
    const mockFirebase: FirebaseMock = new FirebaseMock();
    const mockNavControl: LocationMock = new LocationMock();
    const navData = {'title': 'Rice with Toppings', 'search': true};

    let popMock;

    let firebaseInitialize;
    let firebaseAuth;
    let firebaseConnect;

    beforeAll(() => {
        firebaseInitialize = spyOn(firebase, 'initializeApp').and.returnValue(mockFirebase.initializeApp({}));
        firebaseAuth = spyOn(firebase, 'auth').and.returnValue(mockFirebase.auth());
        firebaseConnect = spyOn(firebase, 'database').and.returnValue(mockFirebase.database());
    });

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [NavbarPage],
            schemas: [CUSTOM_ELEMENTS_SCHEMA],
            providers: [{provide: CookieService, useClass: CookieServiceMock}, {
                provide: PopoverController,
                useClass: PopupMock
            }, {provide: Location, useValue: mockNavControl}]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(NavbarPage);
        component = fixture.componentInstance;
        component.user = mockFirebase.user;
        fixture.detectChanges();

        debugElement = fixture.debugElement.query(By.css('ion-header'));
        titleElement = fixture.debugElement.query(By.css('ion-header ion-title'));
        searchElement = fixture.debugElement.query(By.css('ion-header ion-searchbar'));
        popMock = spyOn(component, 'showUserPopup');
    });

    it('should only be displayed if user information is available', () => {
        expect(component.user).toEqual(mockFirebase.user);
        expect(debugElement).toBeTruthy();
        expect(titleElement.nativeElement.textContent.trim()).toBe('Food Recipe');
        expect(searchElement).toBeFalsy();
    });

    it('should only be displayed if user information is available', () => {
        component.title = navData.title;
        component.search = navData.search;
        fixture.detectChanges();
        searchElement = fixture.debugElement.query(By.css('ion-header ion-searchbar'));
        expect(component.user).toEqual(mockFirebase.user);
        expect(debugElement).toBeTruthy();
        expect(titleElement.nativeElement.textContent.trim()).toBe(navData.title);
        expect(searchElement).toBeTruthy();
    });

    it('should show popup on click', () => {
        const buttonElement = fixture.debugElement.nativeElement.querySelector('#popup');
        buttonElement.click();
        fixture.detectChanges();
        fixture.whenStable().then(() => {
            expect(popMock).toHaveBeenCalled();
        });
    });
});
