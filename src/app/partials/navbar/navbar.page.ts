import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChange, SimpleChanges} from '@angular/core';
import {Users} from '../../model/users/users';
import {ExtrasService} from '../../providers/extras/extras.service';
import {Location} from '@angular/common';

@Component({
    selector: 'app-navbar',
    templateUrl: './navbar.page.html',
    styleUrls: ['./navbar.page.scss'],
})
export class NavbarPage implements OnInit {

    @Input() user: Users;
    @Input() title = 'Food Recipe';
    @Input() search = false;
    @Input() back = false;
    @Output() searchValue = new EventEmitter<String>();
    private _searchValue = '';


    constructor(public extraService: ExtrasService) {
    }

    ngOnInit() {
    }

    async goBack(event) {
        this.extraService.goBack();
    }

    async showUserPopup(event) {
        const popover = await this.extraService.showPopup(event, {type: 'signout', user: this.user}, false);
    }

    onInput(event) {
        const val = event.target.value;
        this.searchValue.emit(val);
    }

    onCancel(event) {
        event.target.value = '';
        this.searchValue.emit('');
    }

}
